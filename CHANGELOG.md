# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.5.12 - 2024-09-27(13:59:36 +0000)

### Other

- [DNSSD] Scanning not working on AP

## Release v0.5.11 - 2024-09-10(07:10:18 +0000)

### Other

- [AppArmor] Create AppAmor profile for plugins

## Release v0.5.10 - 2024-08-28(21:44:53 +0000)

### Other

- TR-181. Device.DNS.SD. data model issues 19.03.2024

## Release v0.5.9 - 2024-08-06(08:19:51 +0000)

### Other

- add support for multiple runlevels

## Release v0.5.8 - 2024-07-23(07:49:22 +0000)

### Fixes

- Better shutdown script

## Release v0.5.7 - 2024-07-09(07:02:19 +0000)

### Other

- - [tr181-dnssd] umdns is sometime launch too soon when there is a wifi interface

## Release v0.5.6 - 2024-07-08(07:05:01 +0000)

## Release v0.5.5 - 2024-07-04(14:13:17 +0000)

### Other

- - [tr181-dnssd] umdns is sometime launch too soon when there is a wifi interface

## Release v0.5.4 - 2024-06-18(14:14:16 +0000)

### Other

- - [tr181-dnssd] strengthening of tr181-dnssd plugin

## Release v0.5.3 - 2024-06-17(15:31:21 +0000)

### Fixes

- amx plugin should not run as root user

## Release v0.5.2 - 2024-05-22(21:10:44 +0000)

### Fixes

- [tr181-dnssd] restart back-end when it crashes

## Release v0.5.1 - 2024-04-10(07:07:32 +0000)

### Changes

- Make amxb timeouts configurable

## Release v0.5.0 - 2024-03-23(13:07:50 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.4.13 - 2024-03-19(12:59:13 +0000)

### Other

- Rework TR-181 interface stacks indexing number

## Release v0.4.12 - 2024-01-16(11:29:28 +0000)

### Other

- Set `lla` controllers by default

## Release v0.4.11 - 2023-10-13(14:13:06 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v0.4.10 - 2023-09-06(07:31:46 +0000)

## Release v0.4.9 - 2023-08-24(10:14:52 +0000)

### Fixes

- [umdns][TR181-DNSSD] When changing the LAN IP address, umdns is not correctly restarted

## Release v0.4.8 - 2023-08-21(09:47:58 +0000)

### Fixes

- [tr181-dnssd] change the way of default loading

## Release v0.4.7 - 2023-08-11(08:41:38 +0000)

### Fixes

- Device.DNS.SD incorrect data types found

## Release v0.4.6 - 2023-08-07(07:55:21 +0000)

### Fixes

- TR181-DNSSD episodically fails to start umdns process and can't advertise

## Release v0.4.5 - 2023-04-20(13:12:34 +0000)

### Fixes

- service protocol should be integers

## Release v0.4.4 - 2023-03-17(18:43:29 +0000)

### Other

- [baf] Correct typo in config option

## Release v0.4.3 - 2023-03-16(14:25:29 +0000)

### Other

- Add AP config files

## Release v0.4.2 - 2023-03-15(11:09:43 +0000)

### Fixes

- [tr181-dnssd] restarting backend when adding new advertissing service crash umdns process

## Release v0.4.1 - 2023-03-09(09:17:28 +0000)

### Other

- Add missing runtime dependency on rpcd
- [Config] enable configurable coredump generation

## Release v0.4.0 - 2023-02-21(15:57:05 +0000)

### Other

- [PRPL] Make MSS work on prpl config

## Release v0.3.12 - 2023-02-20(09:49:33 +0000)

### Fixes

- [tr181-dnssd] DNSSD is not enabled

## Release v0.3.11 - 2023-01-10(11:04:58 +0000)

### Fixes

- [tr181-dnssd] empty the umdns start script as the dnssd plugin start it

## Release v0.3.10 - 2023-01-09(10:18:34 +0000)

### Fixes

- avoidable copies of strings, htables and lists

## Release v0.3.9 - 2023-01-04(09:48:51 +0000)

### Other

- [tr181-dnssd] configuration of umdsn without uci [add]

## Release v0.3.8 - 2022-12-22(09:54:44 +0000)

### Fixes

- [tr181-dnssd] combine the two module in one for uci

## Release v0.3.7 - 2022-12-19(16:14:53 +0000)

### Other

- [tr181-dnssd] creat rpc methode to allow the subscription to a service [add]

## Release v0.3.6 - 2022-12-09(09:16:04 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v0.3.5 - 2022-12-02(17:19:21 +0000)

### Fixes

- [USP][CDROUTER] mDNS discovery is not supported by a USP Agent

## Release v0.3.4 - 2022-12-01(14:46:55 +0000)

### Fixes

- [DNSSD] Only check Enable for the open and close of the firewall

## Release v0.3.3 - 2022-11-10(12:02:22 +0000)

### Fixes

- Issue : HOP-2199 [CDRouter][IPv4][VLAN] FAIL: mDNS lookup for prplOS.local failed

## Release v0.3.2 - 2022-10-24(10:54:09 +0000)

### Other

- tr181-dnssd: mDNS service is not available over IPv6

## Release v0.3.1 - 2022-09-06(06:49:43 +0000)

### Fixes

- [tr181-dnssd] add Documentation and README

## Release v0.3.0 - 2022-08-25(08:00:22 +0000)

### New

- [tr181-dnssd] add advertisement datamodel

## Release v0.2.3 - 2022-08-23(14:57:32 +0000)

### Other

- Opensource component

## Release v0.2.2 - 2022-08-23(08:55:18 +0000)

### Other

- [tr181-dnssd] add unit test [add]

## Release v0.2.1 - 2022-08-11(07:39:29 +0000)

### Fixes

- [tr181-dnssd] modify umdns to allow host in services call

## Release v0.2.0 - 2022-08-08(09:47:57 +0000)

### New

- [tr181-dnssd] Add umdnssd module

## Release v0.1.0 - 2022-07-27(11:15:31 +0000)

### New

- [tr181-dnssd] Creation of a data model compatible with tr181

