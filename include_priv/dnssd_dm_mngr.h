/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__DNSSD_DM_MNGR_H__)
#define __DNSSD_DM_MNGR_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <amxc/amxc.h>
#include <amxo/amxo.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxm/amxm.h>

#include "dnssd_info.h"
#include "dnssd_controller.h"

typedef enum {
    DNSSD_DM_MNGR_EVENT_RESTART = 1 << 0,
    DNSSD_DM_MNGR_EVENT_RECONFIGURE = 1 << 1,
    DNSSD_DM_MNGR_EVENT_RELOAD = 1 << 2,
} dnssd_dm_mngr_event_flags_t;

typedef struct {
    dnssd_dm_mngr_event_flags_t flags;
    amxp_timer_t* bundle_timer;
} dnssd_dm_mngr_events_t;

typedef struct {
    dnssd_info_t* info;
    amxm_module_t* mod;
    dnssd_dm_mngr_events_t events;
    bool back_end_started;
} dnssd_dm_mngr_priv_t;

int dnssd_dm_mngr_init(void);
int dnssd_dm_mngr_clean(void);

int dnssd_dm_mngr_restart(const char* function_name,
                          amxc_var_t* args,
                          amxc_var_t* ret);

dnssd_info_t* dnssd_dm_mngr_get_info(amxd_object_t* dnssd);
dnssd_dm_mngr_priv_t* dnnsd_obj_get_priv(amxd_object_t* dnssd);

void dnssd_dm_mngr_emit_event(amxd_object_t* dnssd, dnssd_dm_mngr_event_flags_t event);
void dnssd_dm_mngr_update_status(amxd_object_t* dnssd);

// service related
int dm_mngr_set_services(const char* function_name,
                         amxc_var_t* args,
                         amxc_var_t* ret);

int dm_remove_service(amxd_object_t* templ,
                      amxd_object_t* service,
                      void* priv);

#ifdef __cplusplus
}
#endif

#endif // __DNSSD_DM_MNGR_H__