/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxm/amxm.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxb/amxb.h>

#include "umdns_utils.h"

#define DEFAULT_TIMER 20000

#define ME "dnssd-umdns"

/**
 * @brief
 * Parse a text entry.
 *
 * @param htable The variant htable to add the entry to.
 * @param entry The text entry to add given by umdns.
 */
static void parse_entry(amxc_var_t* htable, amxc_var_t* entry) {
    char* key = NULL;
    char* value = NULL;
    char* entry_string = NULL;

    when_null_trace(htable, exit, ERROR, "No list given.");
    when_null_trace(entry, exit, ERROR, "No text entry to add.");

    entry_string = amxc_var_dyncast(cstring_t, entry);
    key = strtok(entry_string, "=");
    value = strtok(NULL, "=");
    amxc_var_add_key(cstring_t, htable, key, value);
exit:
    free(entry_string);
    return;
}

/**
 * @brief
 * Construct a csv list of all the ip address of a target
 *
 * @param buffer The buffer to store the csv list.
 * @param host_name the target field of a SRV MDNS reply.
 */
static char* construct_hosts(const char* host_name) {
    amxc_var_t ret;
    amxc_string_t buffer;
    amxc_var_t* hosts_list = NULL;
    amxc_var_t* host;
    amxb_bus_ctx_t* ctx = NULL;
    const char* ipv4 = NULL;
    const char* ipv6 = NULL;
    char* ret_val = NULL;

    amxc_var_init(&ret);
    amxc_string_init(&buffer, 0);

    when_str_empty(host_name, exit);

    ctx = amxb_be_who_has("umdns.");
    when_null_trace(ctx, exit, ERROR, "Cannot get the bus of umdns.");
    when_failed_trace(amxb_call(ctx, "umdns.", "hosts", NULL, &ret, 100), exit, ERROR, "Unable to get the hosts from umdns.");

    hosts_list = amxc_var_get_first(&ret);
    when_null_trace(hosts_list, exit, ERROR, "No list in umdns.browse().");

    host = GET_ARG(hosts_list, host_name);
    ipv4 = GET_CHAR(host, "ipv4");
    ipv6 = GET_CHAR(host, "ipv6");

    if(ipv4 != NULL) {
        amxc_string_appendf(&buffer, "%s", ipv4);
    }
    if(ipv6 != NULL) {
        amxc_string_appendf(&buffer, "%s%s", (amxc_string_get(&buffer, 0)[0] != '\0') ? "," : "", ipv6);
    }
    ret_val = amxc_string_take_buffer(&buffer);
exit:
    amxc_string_clean(&buffer);
    amxc_var_clean(&ret);
    return ret_val;
}

/**
 * @brief
 * Parse the service_name given in the service to creat an service entry for the plugin.
 *
 * @param dest The variant list to add the service to.
 * @param service_name The service_name with its parameters from umdns to add.
 * @param protocol The service application protocol.
 * @param transport_protocol The service transport protocol.
 */
static void parse_service_name(amxc_var_t* dest, amxc_var_t* service_name, char* protocol, char* transport_protocol) {
    amxc_var_t* data = NULL;
    amxc_var_t* txt_list = NULL;
    amxc_var_t* text_record = NULL;

    when_null_trace(dest, exit, ERROR, "No hashtable given.");
    when_null_trace(service_name, exit, ERROR, "No service_name given.");
    when_null_trace(protocol, exit, ERROR, "No protocol given.");
    when_null_trace(transport_protocol, exit, ERROR, "No transport protocol given.");

    data = amxc_var_add(amxc_htable_t, dest, NULL);

    amxc_var_add_key(cstring_t, data, "ApplicationProtocol", &protocol[1]);
    amxc_var_add_key(cstring_t, data, "TransportProtocol", strcmp("_tcp", transport_protocol) == 0 ? "TCP" : "UDP");

    amxc_var_add_key(cstring_t, data, "InstanceName", amxc_var_key(service_name));
    if(GET_CHAR(service_name, "domain") != NULL) {
        amxc_var_add_key(cstring_t, data, "Domain", GET_CHAR(service_name, "domain"));
    } else {
        amxc_var_add_key(cstring_t, data, "Domain", "");
    }
    if(GET_CHAR(service_name, "target") != NULL) {
        char* host = construct_hosts(GET_CHAR(service_name, "target"));
        amxc_var_add_key(csv_string_t, data, "Host", host);
        amxc_var_add_key(cstring_t, data, "Target", GET_CHAR(service_name, "target"));
        free(host);
    } else {
        amxc_var_add_key(csv_string_t, data, "Host", "");
    }
    amxc_var_add_key(uint32_t, data, "Port", GET_UINT32(service_name, "port"));

    txt_list = GET_ARG(service_name, "txt");
    when_null_trace(txt_list, exit, WARNING, "No text entries given.");
    text_record = amxc_var_add_key(amxc_htable_t, data, "TextRecord", NULL);
    amxc_var_for_each(txt, txt_list) {
        parse_entry(text_record, txt);
    }
exit:
    return;
}

/**
 * @brief
 * Parse the service data from umdns to be plugin compatible.
 *
 * @param dest The variant list to add the service to.
 * @param service The service from umdns to add.
 */
static void parse_service(amxc_var_t* dest, amxc_var_t* service) {
    char* transport_protocol = NULL;
    char* protocol = NULL;
    const char* key = NULL;
    char* key_mod = NULL;

    when_null_trace(dest, exit, ERROR, "No hashtable given.");
    when_null_trace(service, exit, ERROR, "No service to add.");

    key = amxc_var_key(service);
    key_mod = (char*) malloc(strlen(key) + 1);
    strcpy(key_mod, key);
    protocol = strtok(key_mod, ".");
    transport_protocol = strtok(NULL, ".");

    amxc_var_for_each(service_name, service) {
        parse_service_name(dest, service_name, protocol, transport_protocol);
    }

exit:
    free(key_mod);
    return;
}

/**
 * @brief
 * Return the local services from the umdns server interface.
 *
 * @param local_timer The timer that call back this function.
 * @param priv private information pass through calls.
 */
void umdns_get_services(UNUSED amxp_timer_t* local_timer,
                        UNUSED void* priv) {
    amxc_var_t ret_call;
    amxc_var_t data;
    amxc_var_t* services_list = NULL;
    amxb_bus_ctx_t* ctx = NULL;
    int ret = 0;

    amxc_var_init(&ret_call);
    amxc_var_init(&data);

    amxc_var_set_type(&data, AMXC_VAR_ID_LIST);

    ctx = amxb_be_who_has("umdns.");
    when_null_trace(ctx, exit, ERROR, "Cannot get the bus of umdns.");
    ret = amxb_call(ctx, "umdns.", "browse", NULL, &ret_call, 100);
    when_failed_trace(ret, exit, ERROR, "Unable to get the services from umdns (%d).", ret);

    services_list = amxc_var_get_first(&ret_call);
    when_null_trace(services_list, exit, ERROR, "No list in umdns.browse().");

    amxc_var_for_each(service, services_list) {
        parse_service(&data, service);
    }

    umdns.send_services(&data);
exit:
    amxp_timer_start(umdns.timer_set_service, DEFAULT_TIMER);
    amxc_var_clean(&data);
    amxc_var_clean(&ret_call);
    return;
}

/**
 * @brief
 * Update the umdns service table.
 *
 * @param local_timer The timer that call back this function.
 * @param priv private information pass through calls.
 */
void umdns_update_services(UNUSED amxp_timer_t* local_timer,
                           UNUSED void* priv) {
    amxc_var_t ret_call;
    amxb_bus_ctx_t* ctx = NULL;
    int ret = 0;

    amxc_var_init(&ret_call);

    ctx = amxb_be_who_has("umdns.");
    when_null_trace(ctx, exit, ERROR, "Cannot get the bus of umdns.");
    ret = amxb_call(ctx, "umdns.", "update", NULL, &ret_call, 100);
    when_failed_trace(ret, exit, ERROR, "Unable to update the services of umdns. (%d)", ret);

exit:
    amxp_timer_start(umdns.timer_update, DEFAULT_TIMER);
    amxc_var_clean(&ret_call);
    return;
}
