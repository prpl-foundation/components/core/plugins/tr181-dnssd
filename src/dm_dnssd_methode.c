/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "dm_dnssd.h"
#include "dnssd.h"
#include "dnssd_info.h"

#define ME "dnssd"

static const char filter[] = \
    "notification in ['dm:instance-added', 'dm:instance-removed'] || "
    "contains(parameters.Value) ";

/**
 * @brief
 * Subscribe to a service when it is added/updated/removed depending of its InstanceName, ApplicationProtocol and TransportProtocol.
 *
 * @param object The DNSSD object.
 * @param func The function called.
 * @param args The data to creat the filter.
 * @param ret The return variable.
 * @return amxd_status_t 0 if the filter is added.
 */
amxd_status_t _service_subscribe(amxd_object_t* object,
                                 UNUSED amxd_function_t* func,
                                 amxc_var_t* args,
                                 amxc_var_t* ret) {
    amxd_status_t retval = amxd_status_unknown_error;
    amxc_llist_t paths;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("DNSSD.");
    amxd_dm_t* dm = amxd_object_get_dm(object);
    const char* key = GET_CHAR(args, "key");
    const char* application_protocol = GET_CHAR(args, "application_protocol");
    const char* transport_protocol = GET_CHAR(args, "transport_protocol");
    dnssd_info_t* info = dnssd_dm_mngr_get_info(object);

    amxc_llist_init(&paths);

    when_null_trace(info, exit, ERROR, "No private information found for DNSSD.");
    when_null_trace(dm, exit, ERROR, "No data model found for DNSSD.");
    when_str_empty_trace(key, exit, ERROR, "No key given for subscription.");
    when_str_empty_trace(application_protocol, exit, ERROR, "No application protocol given for subscription");
    when_str_empty_trace(transport_protocol, exit, ERROR, "No transport protocol given for subscription");
    when_null_trace(ctx, exit, ERROR, "Bus of DNSSD. not found.");

    retval = dnssd_info_create_subscription(info, key, application_protocol, transport_protocol);
    when_false_trace(retval == 0, exit, ERROR, "Failed to create a subscription - key:[%s]", key);

    if(!info->flags.bit.sub_ok) {
        retval = amxb_subscribe(ctx, "DNSSD.Service.", filter, dnssd_send_service, info);
        when_false_trace(retval == 0, exit, ERROR, "Unable to subscribe to services.");
        info->flags.bit.sub_ok = 1;
    }

    amxd_object_resolve_pathf(object, &paths, ".Service.[ApplicationProtocol=='%s' && TransportProtocol=='%s']", application_protocol, transport_protocol);
    amxc_llist_for_each(it, &paths) {
        amxc_var_t data;
        amxd_path_t path;
        char* instance = NULL;
        amxc_var_t* params = NULL;

        amxd_path_init(&path, amxc_string_get(amxc_string_from_llist_it(it), 0));
        amxc_var_init(&data);
        amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

        instance = amxd_path_get_last(&path, true);
        amxc_var_add_key(cstring_t, &data, "path", amxd_path_get(&path, AMXD_OBJECT_TERMINATE));
        amxc_var_add_key(cstring_t, &data, "name", instance);
        amxc_var_add_key(cstring_t, &data, "notification", "dm:instance-added");
        params = amxc_var_add_new_key(&data, "parameters");
        amxc_var_set_type(params, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, params, "ApplicationProtocol", application_protocol);
        amxc_var_add_key(cstring_t, params, "TransportProtocol", transport_protocol);
        dnssd_send_service(NULL, &data, info);

        free(instance);
        instance = NULL;
        amxd_path_clean(&path);
        amxc_var_clean(&data);
    }

exit:
    amxc_llist_clean(&paths, amxc_string_list_it_free);
    amxc_var_set(uint32_t, ret, retval);
    return retval;
}

/**
 * @brief
 * Remove a filter to stop subscribing to a service.
 *
 * @param object The DNSSD object.
 * @param func The function called.
 * @param args The key of the filter.
 * @param ret The return variant.
 * @return amxd_status_t 0 if the filter is removed.
 */
amxd_status_t _service_unsubscribe(amxd_object_t* object,
                                   UNUSED amxd_function_t* func,
                                   amxc_var_t* args,
                                   amxc_var_t* ret) {
    amxd_status_t retval = amxd_status_unknown_error;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("DNSSD.");
    dnssd_info_t* info = dnssd_dm_mngr_get_info(object);
    amxd_dm_t* dm = amxd_object_get_dm(object);
    const char* key = GET_CHAR(args, "key");

    when_null_trace(dm, exit, ERROR, "No data model found for DNSSD.");
    when_str_empty_trace(key, exit, ERROR, "No key given for subscription.");
    when_null_trace(ctx, exit, ERROR, "Bus of DNSSD.Services. not found.");
    when_null_trace(info, exit, ERROR, "No private information found for DNSSD.");

    retval = dnssd_info_delete_subscription(info, key);
    when_false_trace(retval == 0, exit, ERROR, "Failed to delete the subscription - key:[%s]", key);

    if(info->flags.bit.sub_ok && (info->nb_serv_sub == 0)) {
        retval = amxb_unsubscribe(ctx, "DNSSD.Service.", dnssd_send_service, info);
        when_false_trace(retval == 0, exit, ERROR, "Unable to unsubscribe to services.");
        info->flags.bit.sub_ok = 0;
    }
exit:
    amxc_var_set(uint32_t, ret, retval);
    return retval;
}
