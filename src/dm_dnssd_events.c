/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>

#include "dm_dnssd.h"
#include "dnssd.h"
#include "dnssd_dm_mngr.h"
#include "dnssd_info.h"

#define ME "dnssd"

/**
 * @brief
 * Send an event to notify the system that a new service has been discovered.
 *
 * @param service The Service to notify object.
 * @param filter The filter used to select that service.
 * @param dnssd The dnssd object.
 */
static void notify_service(amxd_object_t* service, amxc_var_t* filter, amxd_object_t* dnssd, bool removed) {
    amxc_var_t data;
    amxc_var_t* service_params = NULL;
    amxc_var_t* text_records = NULL;

    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    when_null_trace(filter, exit, ERROR, "No filter given.");
    when_null_trace(dnssd, exit, ERROR, "No DNSSD object given.");

    amxc_var_add_key(cstring_t, &data, "subscription_key", amxc_var_key(filter));
    amxc_var_add_key(bool, &data, "removed", removed);

    if(service) {
        service_params = amxc_var_add_key(amxc_htable_t, &data, "service_object", NULL);
        amxd_object_get_params(service, service_params, amxd_dm_access_public);

        text_records = amxc_var_add_key(amxc_llist_t, &data, "text_records", NULL);
        amxd_object_for_each(instance, itl, amxd_object_findf(service, ".TextRecord.")) {
            amxd_object_t* text_record = amxc_container_of(itl, amxd_object_t, it);
            amxc_var_t* entry = amxc_var_add(amxc_htable_t, text_records, NULL);
            amxd_object_get_params(text_record, entry, amxd_dm_access_public);
        }
    }

    SAH_TRACEZ_INFO(ME, "send event");
    amxd_object_emit_signal(dnssd, "Scan", &data);
exit:
    amxc_var_clean(&data);
    return;
}


/**
 * @brief
 * Find a service that matches the application and transport protocol and send a service discover event if they match.
 *
 * @param dnssd The dnssd object.
 * @param datapath The event data.
 * @param filter The filter used to select that service.
 */
static void notify_matching_service(amxd_object_t* dnssd, const amxc_var_t* const data, amxc_var_t* filter) {
    const char* notification = GET_CHAR(data, "notification");
    when_null_trace(notification, exit, ERROR, "No notification found in the data event.");

    const char* path = GET_CHAR(data, "path");
    when_null_trace(path, exit, ERROR, "No path found in the data event.");
    if(path && (strcmp(path, "DNSSD.Service.") != 0)) {
        return;
    }

    const char* name = GET_CHAR(data, "name");
    when_null_trace(name, exit, ERROR, "No name found in the data event.");

    amxc_var_t* params = GET_ARG(data, "parameters");
    const char* pAppProto = GET_CHAR(params, "ApplicationProtocol");
    const char* pTransProto = GET_CHAR(params, "TransportProtocol");
    const char* fAppProto = GET_CHAR(filter, "app_proto");
    const char* fTransProto = GET_CHAR(filter, "trans_proto");

    if(pAppProto && fAppProto && (strcmp(pAppProto, fAppProto) == 0) &&
       pTransProto && fTransProto && (strcmp(pTransProto, fTransProto) == 0)) {
        amxd_object_t* service = amxd_dm_findf(dnssd_get_dm(), "DNSSD.Service.%s", name);
        notify_service(service, filter, dnssd, strcmp(notification, "dm:instance-removed") == 0);
    }
exit:
    return;
}

/**
 * @brief
 * Check if an event on a service need to be broadcast to the system.
 *
 * @param sig_name The name of the signal.
 * @param data The event data.
 * @param priv The DNSSD private data information.
 */
void dnssd_send_service(UNUSED const char* const sig_name,
                        const amxc_var_t* const data,
                        void* const priv) {
    dnssd_info_t* info = (dnssd_info_t*) priv;
    amxd_object_t* dnssd = NULL;

    when_null_trace(info, exit, ERROR, "No DNSSD private data found.");
    dnssd = info->obj;
    when_null_trace(dnssd, exit, ERROR, "No DNSSD object found.");

    amxc_var_for_each(filter, info->sub_htable) {
        notify_matching_service(dnssd, data, filter);
    }
exit:
    return;
}

/**
 * @brief
 * Event handler function when there is a modification to the enable field of the DNSSD data model.
 *
 * @param event_name Name of the event send to the object, not used.
 * @param event_data data from the called event.
 * @param priv Additional arguments, not used.
 */
void _dnssd_enable_changed(UNUSED const char* const event_name,
                           UNUSED const amxc_var_t* const event_data,
                           UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_dm_t* dm = dnssd_get_dm();
    amxd_object_t* dnssd = NULL;
    bool enable = false;
    dnssd_dm_mngr_priv_t* priv_data = NULL;

    SAH_TRACEZ_INFO(ME, "In enable_changed handler event function.");

    when_null_trace(dm, exit, ERROR, "Cannot get DNSSD data model object.");
    dnssd = amxd_dm_signal_get_object(dm, event_data);
    when_null_trace(dnssd, exit, ERROR, "Unable to get the modified DNSSD object.");
    priv_data = dnnsd_obj_get_priv(dnssd);
    when_null_trace(priv_data, exit, ERROR, "Unable to private data of dnssd object.");

    enable = amxd_object_get_value(bool, dnssd, "Enable", NULL);

    if(priv_data->info == NULL) {
        dnssd_info_create(&priv_data->info, dnssd);
    }
    enable ? dnssd_info_enable(priv_data->info) : dnssd_info_disable(priv_data->info);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

/**
 * @brief
 * Event handler function when the listening interfaces of the dnssd data model change.
 *
 * @param event_name Name of the event send to the object, not used.
 * @param event_data data from the called event.
 * @param priv Additional arguments, not used.
 */
void _dnssd_interfaces_changed(UNUSED const char* const event_name,
                               UNUSED const amxc_var_t* const event_data,
                               UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_dm_t* dm = dnssd_get_dm();
    amxd_object_t* dnssd = NULL;
    bool enable = false;
    dnssd_dm_mngr_priv_t* priv_data = NULL;

    SAH_TRACEZ_INFO(ME, "In interfaces_changed event handler function.");

    when_null_trace(dm, exit, ERROR, "Cannot get DNSSD data model object.");
    dnssd = amxd_dm_signal_get_object(dm, event_data);
    when_null_trace(dnssd, exit, ERROR, "Unable to get the modified DNSSD object.");
    priv_data = dnnsd_obj_get_priv(dnssd);
    when_null_trace(priv_data, exit, ERROR, "Unable to private data of dnssd object.");

    enable = amxd_object_get_value(bool, dnssd, "Enable", NULL);

    if(priv_data->info != NULL) {
        dnssd_info_disable(priv_data->info);
        dnssd_info_delete(&priv_data->info);
    }
    dnssd_info_create(&priv_data->info, dnssd);
    enable ? dnssd_info_enable(priv_data->info) : dnssd_info_disable(priv_data->info);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

/**
 * @brief
 * Used to debug incoming events
 *
 * @param event_name Name of the event send to the object, not used.
 * @param event_data data from the called event.
 * @param priv Additional arguments, not used.
 */
void _print_event(UNUSED const char* const event_name,
                  const amxc_var_t* const event_data,
                  UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "received event: %s", event_name);
    amxc_var_log(event_data);
}
