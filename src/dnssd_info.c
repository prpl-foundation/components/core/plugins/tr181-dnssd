/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>

#include "dnssd.h"
#include "dnssd_dm_mngr.h"
#include "dnssd_info.h"

#define ME "dnssd"

/**
 * @brief
 * Get the string representation of the current status
 *
 * @param info The interface info object.
 */
const cstring_t dnssd_info_status_str(dnssd_info_t* info) {
    if((info == NULL) || !info->flags.bit.enable) {
        return "Disabled";
    }
    if(info->flags.bit.intfs_ok && info->flags.bit.config_ok) {
        return "Enabled";
    }
    return "Error_Misconfigured";
}

/**
 * @brief
 * Check the status of all listening interface to update the dnssd private information
 *
 * @param info The interface info object.
 */
static void sync_intfs_status(dnssd_info_t* info) {
    when_null_trace(info, exit, ERROR, "Cannot get private info from dnssd.");

    info->flags.bit.intfs_ok = 1;

    for(int i = 0; i < info->intfs_size; i++) {
        bool enable = info->intfs_list[i].flags.bit.intf_alias && info->intfs_list[i].flags.bit.intf_name && (info->intfs_list[i].flags.bit.intf_ipv4 || info->intfs_list[i].flags.bit.intf_ipv6);
        info->flags.bit.intfs_ok &= enable;
    }

exit:
    return;
}

/**
 * @brief
 * Callback function for the ipv4 address. Will update the Status of the dnssd.
 *
 * @param sig_name Signal name, not used.
 * @param data ipv4 address.
 * @param priv Private intf_info_t corresponding to the interface which is registered to listen dnssd messages.
 */
static void interface_changed_ipv4_cb(UNUSED const char* const sig_name,
                                      const amxc_var_t* const data,
                                      void* const priv) {
    intf_info_t* intf_info = (intf_info_t*) priv;
    dnssd_info_t* info = NULL;
    SAH_TRACEZ_INFO(ME, "Callback function for interface ipv4 address query called.");
    const char* ipv4 = GETP_CHAR(data, "0.Address");
    if(ipv4 == NULL) {
        ipv4 = "";
    }

    when_null_trace(intf_info, exit, ERROR, "Unable to retrieve info for the interface.");
    when_null_trace(ipv4, exit, ERROR, "No ipv4 address found in the data event.");

    info = dnssd_dm_mngr_get_info(intf_info->dnssd);
    when_null_trace(info, exit, ERROR, "Unable to retrieve info for the dnssd.");

    if(intf_info->intf_ipv4) {
        if(strcmp(intf_info->intf_ipv4, ipv4) == 0) {
            goto exit;
        } else {
            free(intf_info->intf_ipv4);
            intf_info->intf_ipv4 = NULL;
            intf_info->flags.bit.intf_ipv4 = 0;
        }
    }

    intf_info->intf_ipv4 = strdup(ipv4);
    intf_info->flags.bit.intf_ipv4 = 1;

    sync_intfs_status(info);
    dnssd_dm_mngr_emit_event(info->obj, DNSSD_DM_MNGR_EVENT_RESTART);
exit:
    return;
}

/**
 * @brief
 * Callback function for the ipv6 address. Will update the Status of the dnssd.
 *
 * @param sig_name Signal name, not used.
 * @param data ipv6 address.
 * @param priv Private intf_info_t corresponding to the interface which is registered to listen dnssd messages.
 */
static void interface_changed_ipv6_cb(UNUSED const char* const sig_name,
                                      const amxc_var_t* const data,
                                      void* const priv) {
    intf_info_t* intf_info = (intf_info_t*) priv;
    dnssd_info_t* info = NULL;


    SAH_TRACEZ_INFO(ME, "Callback function for interface ipv6 address query called.");
    const char* ipv6 = GETP_CHAR(data, "0.Address");
    if(ipv6 == NULL) {
        ipv6 = "";
    }

    when_null_trace(intf_info, exit, ERROR, "Unable to retrieve info for the interface.");
    when_null_trace(ipv6, exit, ERROR, "No ipv6 address found in the data event.");

    info = dnssd_dm_mngr_get_info(intf_info->dnssd);
    when_null_trace(info, exit, ERROR, "Unable to retrieve info for the dnssd.");

    if(intf_info->intf_ipv6) {
        if(strcmp(intf_info->intf_ipv6, ipv6) == 0) {
            goto exit;
        } else {
            free(intf_info->intf_ipv6);
            intf_info->intf_ipv6 = NULL;
            intf_info->flags.bit.intf_ipv6 = 0;
        }
    }

    intf_info->intf_ipv6 = strdup(ipv6);
    intf_info->flags.bit.intf_ipv6 = 1;

    sync_intfs_status(info);
    dnssd_dm_mngr_emit_event(info->obj, DNSSD_DM_MNGR_EVENT_RESTART);
exit:
    return;
}

/**
 * @brief
 * Callback function for the NetDevName query. Will update the Status of the dnssd.
 *
 * @param sig_name Signal name, not used.
 * @param data NetDev name if bound.
 * @param priv Private intf_info_t corresponding to the interface which is registered to listen dnssd messages.
 */
static void interface_changed_netname_cb(UNUSED const char* const sig_name,
                                         const amxc_var_t* const data,
                                         void* const priv) {
    intf_info_t* intf_info = (intf_info_t*) priv;
    dnssd_info_t* info = NULL;
    char* intf_name = NULL;
    SAH_TRACEZ_INFO(ME, "Callback function for interface name query called.");

    when_null_trace(data, exit, ERROR, "Data is null.");
    when_null_trace(intf_info, exit, ERROR, "Unable to retrieve info for the interface.");

    info = dnssd_dm_mngr_get_info(intf_info->dnssd);
    intf_info->flags.bit.intf_name = 0;
    when_null_trace(info, exit, ERROR, "Unable to retrieve info for the dnssd.");
    when_null_trace(info->obj, exit, ERROR, "Unable to retrieve the dnssd object.");
    intf_name = amxc_var_dyncast(cstring_t, data);
    when_str_empty_trace(intf_name, sync, WARNING, "Cannot get the interface name from NetModel, waiting to start back-end.");
    free(intf_info->intf_name);
    intf_info->intf_name = intf_name;
    intf_info->flags.bit.intf_name = true;
sync:
    sync_intfs_status(info);
    dnssd_dm_mngr_emit_event(info->obj, DNSSD_DM_MNGR_EVENT_RESTART);
exit:
    return;
}

/**
 * @brief
 * Callback function for the InterfaceAlias query. Will update the Status of the dnssd.
 *
 * @param sig_name Signal name, not used.
 * @param data Interface alias name.
 * @param priv Private intf_info_t corresponding to the interface which is registered to listen dnssd messages.
 */
static void interface_changed_alias_cb(UNUSED const char* const sig_name,
                                       const amxc_var_t* const data,
                                       void* const priv) {
    intf_info_t* intf_info = (intf_info_t*) priv;
    dnssd_info_t* info = NULL;
    char* intf_alias = NULL;
    SAH_TRACEZ_INFO(ME, "Callback function for interface alias query called.");

    when_null_trace(data, exit, ERROR, "Data is null.");
    when_null_trace(intf_info, exit, ERROR, "Unable to retrieve info for the interface.");

    info = dnssd_dm_mngr_get_info(intf_info->dnssd);
    intf_info->flags.bit.intf_alias = 0;
    when_null_trace(info, exit, ERROR, "Unable to retrieve info for the dnssd.");
    when_null_trace(info->obj, exit, ERROR, "Unable to retrieve the dnssd object.");

    intf_alias = amxc_var_dyncast(cstring_t, data);
    when_null_trace(intf_alias, sync, ERROR, "Cannot get the interface Alias from NetModel.");
    free(intf_info->intf_alias);
    intf_info->intf_alias = intf_alias;
    intf_info->flags.bit.intf_alias = 1;
sync:
    sync_intfs_status(info);
    dnssd_dm_mngr_emit_event(info->obj, DNSSD_DM_MNGR_EVENT_RESTART);
exit:
    return;
}

/**
 * @brief
 * Clear the data allocated in a intf_info structure
 *
 * @param info The intf_info data to clear.
 */
static void intf_info_clear(intf_info_t* info) {
    when_null_trace(info, exit, WARNING, "Interface info given is null");
    if(info->q_name != NULL) {
        netmodel_closeQuery(info->q_name);
        info->q_name = NULL;
    }
    if(info->q_alias != NULL) {
        netmodel_closeQuery(info->q_alias);
        info->q_alias = NULL;
    }
    if(info->q_ipv4 != NULL) {
        netmodel_closeQuery(info->q_ipv4);
        info->q_ipv4 = NULL;
    }
    if(info->q_ipv6 != NULL) {
        netmodel_closeQuery(info->q_ipv6);
        info->q_ipv6 = NULL;
    }

    free(info->intf_name);
    free(info->intf_alias);
    free(info->intf_path);
    free(info->intf_ipv4);
    free(info->intf_ipv6);
exit:
    return;
}

/**
 * @brief
 * Clear the data allocated as private dnssd info data.
 *
 * @param obj The dnssd amxd object link to the listening interface.
 */
void dnssd_info_delete(dnssd_info_t** info_p) {
    dnssd_info_t* info = NULL;

    when_null(info_p, exit);

    info = *info_p;
    *info_p = NULL;
    when_null(info, exit);

    for(int i = 0; i < info->intfs_size; i++) {
        intf_info_clear(&(info->intfs_list[i]));
    }
    if(info->last_service_htable != NULL) {
        amxc_var_delete(&(info->last_service_htable));
        info->last_service_htable = NULL;
    }

    info->intfs_size = 0;
    free(info->intfs_list);
    free(info);
exit:
    return;
}

/**
 * @brief
 * Disable the subscription to queries and remove the interface path from local private data.
 *
 * @param info The interface info object.
 */
static void intf_info_disable(intf_info_t* info) {
    SAH_TRACEZ_INFO(ME, "Disabling NetModel query subscription.");

    when_null_trace(info, exit, ERROR, "No interface info given.");

    info->flags.bit.intf_name = 0;
    if(info->q_name != NULL) {
        netmodel_closeQuery(info->q_name);
        info->q_name = NULL;
    }
    info->flags.bit.intf_alias = 0;
    if(info->q_alias != NULL) {
        netmodel_closeQuery(info->q_alias);
        info->q_alias = NULL;
    }
    info->flags.bit.intf_ipv4 = 0;
    if(info->intf_ipv4 != NULL) {
        free(info->intf_ipv4);
        info->intf_ipv4 = NULL;
    }
    if(info->q_ipv4 != NULL) {
        netmodel_closeQuery(info->q_ipv4);
        info->q_ipv4 = NULL;
    }
    info->flags.bit.intf_ipv6 = 0;
    if(info->intf_ipv6 != NULL) {
        free(info->intf_ipv6);
        info->intf_ipv6 = NULL;
    }
    if(info->q_ipv6 != NULL) {
        netmodel_closeQuery(info->q_ipv6);
        info->q_ipv6 = NULL;
    }
exit:
    return;
}

/**
 * @brief
 * Disable the all the subscription queries.
 *
 * @param obj The amxd dnssd object.
 */
void dnssd_info_disable(dnssd_info_t* info) {
    SAH_TRACEZ_INFO(ME, "Disabling NetModel query subscription.");

    when_null_trace(info, exit, ERROR, "Cannot get the private dnssd info from dnssd object.");

    amxd_object_for_all(info->obj, ".Service.*", dm_remove_service, info);
    if(info->last_service_htable != NULL) {
        amxc_var_delete(&(info->last_service_htable));
        info->last_service_htable = NULL;
    }

    for(int i = 0; i < info->intfs_size; i++) {
        intf_info_disable(&(info->intfs_list[i]));
    }

    info->flags.bit.enable = false;

    sync_intfs_status(info);
    dnssd_dm_mngr_emit_event(info->obj, DNSSD_DM_MNGR_EVENT_RESTART);
exit:
    return;
}

/**
 * @brief
 * Open all netmodel queries for the listening interface
 *
 * @param info The interface info structure.
 */
static void intf_info_open_queries(intf_info_t* info) {
    when_null_trace(info, exit, ERROR, "Cannot get the interface info.");

    if(info->q_name != NULL) {
        netmodel_closeQuery(info->q_name);
        info->q_name = NULL;
        info->flags.bit.intf_name = 0;
    }
    if(info->q_alias != NULL) {
        netmodel_closeQuery(info->q_alias);
        info->q_alias = NULL;
        info->flags.bit.intf_alias = 0;
    }
    if(info->q_ipv4 != NULL) {
        netmodel_closeQuery(info->q_ipv4);
        info->q_ipv4 = NULL;
        info->flags.bit.intf_ipv4 = 0;
    }
    if(info->q_ipv6 != NULL) {
        netmodel_closeQuery(info->q_ipv6);
        info->q_ipv6 = NULL;
        info->flags.bit.intf_ipv6 = 0;
    }

    info->q_name = netmodel_openQuery_getFirstParameter((const char*) info->intf_path,
                                                        "dnssd-manager",
                                                        "NetDevName",
                                                        "netdev-up",
                                                        netmodel_traverse_down,
                                                        interface_changed_netname_cb,
                                                        (void*) info);
    if(info->q_name == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to add state query to netmodel");
    }

    info->q_alias = netmodel_openQuery_getFirstParameter((const char*) info->intf_path,
                                                         "dnssd-manager",
                                                         "InterfaceAlias",
                                                         NULL,
                                                         netmodel_traverse_down,
                                                         interface_changed_alias_cb,
                                                         (void*) info);
    if(info->q_alias == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to add alias query to netmodel");
    }

    info->q_ipv4 = netmodel_openQuery_getAddrs((const char*) info->intf_path,
                                               "dnssd-manager",
                                               "ipv4",
                                               netmodel_traverse_down,
                                               interface_changed_ipv4_cb,
                                               info);
    if(info->q_ipv4 == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to add ipv4 query to netmodel");
    }

    info->q_ipv6 = netmodel_openQuery_getAddrs((const char*) info->intf_path,
                                               "dnssd-manager",
                                               "ipv6",
                                               netmodel_traverse_down,
                                               interface_changed_ipv6_cb,
                                               info);
    if(info->q_ipv6 == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to add ipv6 query to netmodel");
    }
exit:
    return;
}

/**
 * @brief
 * Subscribe to netmodel queries and initialise the interface path in the private data of the model.
 *
 * @param info The interface info structure to enable.
 */
static void intf_info_enable(intf_info_t* info) {
    SAH_TRACEZ_INFO(ME, "Enabling NetModel query subscription.");

    when_null_trace(info, exit, ERROR, "Cannot get private interface info data.");

    intf_info_open_queries(info);
exit:
    return;
}

/**
 * @brief
 * Subscribe to netmodel queries and initialise the interfaces path in the private data of the model.
 *
 * @param obj The amxd dnssd object.
 */
void dnssd_info_enable(dnssd_info_t* info) {
    SAH_TRACEZ_INFO(ME, "Enabling NetModel query subscription.");
    when_null_trace(info, exit, ERROR, "Cannot get the private dnssd info from dnssd object.");

    for(int i = 0; i < info->intfs_size; i++) {
        intf_info_enable(&(info->intfs_list[i]));
    }

    info->flags.bit.enable = true;

    sync_intfs_status(info);
    dnssd_dm_mngr_emit_event(info->obj, DNSSD_DM_MNGR_EVENT_RESTART);
exit:
    return;
}

/**
 * @brief
 * Fill data for the interface info, memory should already been allocated.
 *
 * @param info The interface information structure.
 * @param obj The dnssd amxd object link to the listening interface.
 * @param path Path to the interface.
 */
static void intf_info_create(intf_info_t* info, amxd_object_t* obj, const char* path) {
    when_null_trace(info, exit, ERROR, "Cannot allocate memory for interface private data info.");
    when_null_trace(path, exit, ERROR, "Path to the interface should not be NULL");
    info->dnssd = obj;
    info->intf_path = strdup(path);
    info->flags.all_flags = 0;
exit:
    return;
}

/**
 * @brief
 * Allocate memory for private dnssd info data.
 *
 * @param obj The dnssd amxd object link to the listening interface.
 */
void dnssd_info_create(dnssd_info_t** info, amxd_object_t* obj) {
    amxc_llist_t* intfs_path = NULL;
    amxc_var_t params;
    int curs = 0;

    amxc_var_init(&params);

    when_null_trace(info, exit, ERROR, "No dnssd object given");
    when_null_trace(obj, exit, ERROR, "No dnssd object given");

    *info = (dnssd_info_t*) calloc(1, sizeof(dnssd_info_t));
    when_null_trace(*info, exit, ERROR, "Cannot allocate memory for dnssd private data info.");
    (*info)->obj = obj;

    amxd_object_get_param(obj, "AdvertisedInterfaces", &params);
    amxc_var_cast(&params, AMXC_VAR_ID_LIST);
    intfs_path = amxc_var_dyncast(amxc_llist_t, &params);
    when_null_trace(intfs_path, exit, ERROR, "Cannot get interface list.");

    (*info)->intfs_size = amxc_llist_size(intfs_path);
    (*info)->intfs_list = (intf_info_t*) calloc((*info)->intfs_size, sizeof(intf_info_t));

    amxc_llist_for_each(it, intfs_path) {
        amxc_var_t* path = amxc_var_from_llist_it(it);
        const char* str_path = amxc_var_constcast(cstring_t, path);
        intf_info_create(&((*info)->intfs_list[curs]), obj, str_path);
        curs++;
    }
    (*info)->flags.all_flags = 0;
    (*info)->nb_serv_sub = 0;
exit:
    if(intfs_path != NULL) {
        amxc_llist_delete(&intfs_path, variant_list_it_free);
    }
    amxc_var_clean(&params);
    return;
}

int dnssd_info_create_subscription(dnssd_info_t* info, const char* key, const char* app_proto, const char* trans_proto) {
    amxd_status_t retval = amxd_status_unknown_error;

    amxc_var_t* sub_tmp = NULL;
    amxd_dm_t* dm = dnssd_get_dm();

    when_null_trace(dm, error, ERROR, "No data model found for DNSSD.");
    when_null_trace(info, error, ERROR, "No private info given for DNSSD.");
    when_null_trace(key, error, ERROR, "No key given for subscription.");
    when_null_trace(app_proto, error, ERROR, "No application protocol given for subscription.");
    when_null_trace(trans_proto, error, ERROR, "No transport protocol given for subscription.");

    if(info->sub_htable == NULL) {
        amxc_var_new(&(info->sub_htable));
        amxc_var_set_type(info->sub_htable, AMXC_VAR_ID_HTABLE);
    }

    sub_tmp = amxc_var_add_key(amxc_htable_t, info->sub_htable, key, NULL);
    when_null_trace(sub_tmp, exit, WARNING, "Cannot add new subscription to htable - already exists?");

    amxc_var_add_key(cstring_t, sub_tmp, "app_proto", app_proto);
    amxc_var_add_key(cstring_t, sub_tmp, "trans_proto", trans_proto);

    info->nb_serv_sub += 1;

exit:
    retval = amxd_status_ok;
error:
    return retval;
}

int dnssd_info_delete_subscription(dnssd_info_t* info, const char* key) {
    amxd_status_t retval = amxd_status_unknown_error;

    amxc_var_t* sub_tmp = NULL;
    amxd_dm_t* dm = dnssd_get_dm();

    when_null_trace(dm, error, ERROR, "No data model found for DNSSD.");
    when_null_trace(info, error, ERROR, "No private info given for DNSSD.");
    when_null_trace(key, error, ERROR, "No key given for subscription.");

    when_false_trace(info->nb_serv_sub != 0, exit, INFO, "No subscription to delete");

    sub_tmp = GET_ARG(info->sub_htable, key);
    when_null_trace(sub_tmp, exit, WARNING, "Subscription not found.");

    amxc_var_delete(&sub_tmp);
    info->nb_serv_sub -= 1;
    if(info->nb_serv_sub == 0) {
        amxc_var_delete(&(info->sub_htable));
    }

exit:
    retval = amxd_status_ok;
error:
    return retval;
}
