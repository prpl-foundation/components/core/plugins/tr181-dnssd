/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>

#include "dnssd.h"
#include "dnssd_dm_mngr.h"
#include "dnssd_info.h"
#include "firewall_utility.h"

#define ME "dnssd"

/**
 * @brief
 * Get the dnssd private struct from the DNSSD. object
 *
 * @param dnssd The dnssd object
 *
 * @return The dnssd private struct or NULL if none found/created
 */
dnssd_dm_mngr_priv_t* dnnsd_obj_get_priv(amxd_object_t* dnssd) {
    dnssd_dm_mngr_priv_t* priv = NULL;
    when_null_trace(dnssd, exit, ERROR, "No dnssd object given");
    priv = dnssd->priv;
exit:
    return priv;
}

/**
 * @brief
 * Get the dnssd info struct from the DNSSD. object
 *
 * @param dnssd The dnssd object
 *
 * @return The dnssd info struct or NULL if none found/created
 */
dnssd_info_t* dnssd_dm_mngr_get_info(amxd_object_t* dnssd) {
    dnssd_info_t* info = NULL;
    dnssd_dm_mngr_priv_t* priv = dnnsd_obj_get_priv(dnssd);
    when_null_trace(priv, exit, ERROR, "dnssd object has no private data");
    info = priv->info;
exit:
    return info;
}

/**
 * @brief
 * Set the DNSSD.Status value
 *
 * @param dnssd The dnssd object
 * @param status The new dm dnssd status
 */
static void dnssd_dm_mngr_set_status(amxd_object_t* dnssd, const cstring_t status) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_null_trace(dnssd, exit, ERROR, "No dnssd object given");

    amxd_trans_select_object(&trans, dnssd);
    amxd_trans_set_value(cstring_t, &trans, "Status", status);

    when_failed_trace(amxd_trans_apply(&trans, dnssd_get_dm()), exit, ERROR, "Cannot sync DNSSD object.");
exit:
    SAH_TRACE_INFO("Status updated: %s", status);
    amxd_trans_clean(&trans);
}

/**
 * @brief
 * Update the dm status with the current dnssd status
 *
 * @param dnssd The dnssd object
 */
void dnssd_dm_mngr_update_status(amxd_object_t* dnssd) {
    dnssd_info_t* info = dnssd_dm_mngr_get_info(dnssd);
    dnssd_dm_mngr_set_status(dnssd, dnssd_info_status_str(info));
}

/**
 * @brief
 * Apply all events as bundled actions
 *
 * @param dnssd The dnssd object
 * @param priv The dnssd object's private data
 */
static void dnssd_dm_mngr_sync_events(amxd_object_t* dnssd, dnssd_dm_mngr_priv_t* priv) {
    SAH_TRACEZ_IN(ME);
    dnssd_dm_mngr_event_flags_t events = priv->events.flags;
    bool enable = amxd_object_get_value(bool, dnssd, "Enable", NULL);

    if(events & DNSSD_DM_MNGR_EVENT_RESTART) {
        if(priv->back_end_started) {
            dnssd_ctrlr_action(DNSSD_CTRLR_DISABLE);
        }
        if(enable) {
            dnssd_ctrlr_action(DNSSD_CTRLR_ENABLE);
        }
        goto exit;
    }

    if(enable && (events & DNSSD_DM_MNGR_EVENT_RECONFIGURE)) {
        dnssd_ctrlr_action(DNSSD_CTRLR_ENABLE);
        goto exit;
    }

    if(enable && (events & DNSSD_DM_MNGR_EVENT_RELOAD)) {
        dnssd_ctrlr_action(DNSSD_CTRLR_RELOAD);
        goto exit;
    }

exit:
    dnssd_dm_mngr_update_status(dnssd);
    priv->events.flags = 0;
    SAH_TRACEZ_OUT(ME);
    return;
}

/**
 * @brief
 * Callback that sync's events after (resettable) delay
 *
 * @param timer The timer that triggered the callback
 * @param priv Reference to the dnssd object
 */
static void dnssd_dm_mngr_sync_cb(UNUSED amxp_timer_t* timer, void* priv_arg) {
    amxd_object_t* dnssd = priv_arg;

    when_null_trace(dnssd, exit, ERROR, "No dnssd object found");
    when_null_trace(dnssd->priv, exit, ERROR, "dnssd object has no private data");

    dnssd_dm_mngr_sync_events(dnssd, dnssd->priv);
exit:
    return;
}

/**
 * @brief
 * Emit dnssd object event that should trigger after a delay
 *
 * @param dnssd The dnssd object
 * @param event The event that needs to be synced to the controller
 */
void dnssd_dm_mngr_emit_event(amxd_object_t* dnssd, dnssd_dm_mngr_event_flags_t event) {
    SAH_TRACEZ_IN(ME);
    dnssd_dm_mngr_priv_t* priv = NULL;

    when_null_trace(dnssd, exit, ERROR, "No dnssd object given");
    priv = dnssd->priv;
    when_null_trace(priv, exit, ERROR, "No dnssd object given");

    SAH_TRACE_INFO("incoming event: %d [ %s%s%s]", event,
                   event & DNSSD_DM_MNGR_EVENT_RESTART ? "\"restart\" " : "",
                   event & DNSSD_DM_MNGR_EVENT_RELOAD ? "\"reload\" " : "",
                   event & DNSSD_DM_MNGR_EVENT_RECONFIGURE ? "\"reconfigure\" " : "");
    priv->events.flags |= event;
    when_failed_trace(amxp_timer_start(priv->events.bundle_timer, 1000), exit, ERROR, "Failed to (re)start timer");
exit:
    return;
}

/**
 * @brief
 * Restart/reload DNSSD backend.
 *
 * @return int 0
 */
int dnssd_dm_mngr_restart(UNUSED const char* function_name,
                          UNUSED amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    amxd_object_t* dnssd = amxd_dm_findf(dnssd_get_dm(), "DNSSD.");
    when_null_trace(dnssd, exit, ERROR, "No dnssd object found.");
    dnssd_dm_mngr_emit_event(dnssd, DNSSD_DM_MNGR_EVENT_RESTART);
exit:
    return 0;
}

static int dnssd_dm_mngr_init_module(amxm_module_t** mod) {
    int retval = 1;
    amxm_shared_object_t* so = amxm_get_so("self");

    when_null_trace(mod, exit, ERROR, "No mod reference given");
    when_null_trace(so, exit, ERROR, "Unable to get shared object.");

    when_failed_trace(amxm_module_register(mod, so, MOD_DM_MNGR), exit, ERROR, "Failed to register self module");

    amxm_module_add_function(*mod, "set-service", dm_mngr_set_services);
    amxm_module_add_function(*mod, "restart-backend", dnssd_dm_mngr_restart);

    retval = 0;
exit:
    return retval;
}

/**
 * @brief
 * Initialise DNSSD manager.
 *
 * @return int 0 when initialisation is successful.
 */
int dnssd_dm_mngr_init(void) {
    int retval = 1;
    amxd_object_t* dnssd = amxd_dm_findf(dnssd_get_dm(), "DNSSD.");
    dnssd_dm_mngr_priv_t* priv = NULL;

    when_null_trace(dnssd, exit, ERROR, "No dnssd object found.");

    priv = (dnssd_dm_mngr_priv_t*) calloc(1, sizeof(dnssd_dm_mngr_priv_t));
    when_null_trace(priv, exit, ERROR, "Cannot allocate memory for dnssd private data");
    dnssd->priv = priv;

    when_failed_trace(amxp_timer_new(&priv->events.bundle_timer, dnssd_dm_mngr_sync_cb, dnssd),
                      exit, ERROR, "Failed to create sync timer");
    when_failed_trace(dnssd_dm_mngr_init_module(&priv->mod),
                      exit, ERROR, "Failed to initialize module");
    when_failed_trace(dnssd_ctrlr_init(),
                      exit, ERROR, "Failed to setup controller");
    retval = 0;
exit:
    return retval;
}

/**
 * @brief
 * clean DNSSD manager.
 *
 * @return int 0 when cleaning is successful.
 */
int dnssd_dm_mngr_clean(void) {
    int retval = 1;
    amxd_object_t* dnssd = amxd_dm_findf(dnssd_get_dm(), "DNSSD.");
    dnssd_dm_mngr_priv_t* priv = NULL;

    when_null(dnssd, exit);
    when_null(dnssd->priv, exit);
    priv = dnssd->priv;

    if(priv->info != NULL) {
        dnssd_info_disable(priv->info);
        dnssd_ctrlr_action(DNSSD_CTRLR_DISABLE);
        dnssd_info_delete(&priv->info);
    }

    amxp_timer_delete(&priv->events.bundle_timer);
    amxm_module_deregister(&priv->mod);

    dnssd->priv = NULL;
    free(priv);
    retval = 0;
exit:
    return retval;
}
