/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "firewall_utility.h"
#include "dnssd_info.h"

#define ME "fwl"

/**
 * @brief
 * Adds a firewall rule to open the DNSSD port on an interface.
 * Calls fw.set_service function.
 *
 * @param info The private data from the interface to open the port on.
 */
void firewall_open_dnssd_port(intf_info_t* info) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* tmp = NULL;
    amxc_string_t alias_str;
    amxd_status_t status = amxd_status_unknown_error;
    char* controller = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&alias_str, 0);

    when_null_trace(info, exit, ERROR, "No private info given for the interface.");
    when_null_trace(info->intf_path, exit, ERROR, "Unable to get interface path");
    when_null_trace(info->intf_alias, exit, ERROR, "Unable to get interface alias");

    SAH_TRACEZ_INFO(ME, "Opening firewall port '%d' for interface '%s'...", DNSSD_PORT, info->intf_path);

    amxc_string_setf(&alias_str, FW_ALIAS_PREFIX "%s", info->intf_alias);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    tmp = amxc_var_add_new_key(&args, "id");
    amxc_var_push(cstring_t, tmp, amxc_string_take_buffer(&alias_str));
    amxc_var_add_key(uint32_t, &args, "destination_port", DNSSD_PORT);
    amxc_var_add_key(bool, &args, "enable", true);
    amxc_var_add_key(cstring_t, &args, "interface", info->intf_path);
    amxc_var_add_key(uint32_t, &args, "ipversion", 0);
    amxc_var_add_key(cstring_t, &args, "protocol", "17");

    controller = amxd_object_get_value(cstring_t, info->dnssd, "FWController", NULL);
    when_null_trace(controller, exit, ERROR, "Cannot get controller from pool.");
    status = amxm_execute_function(controller,
                                   FIREWALL_MODULE,
                                   "set_service",
                                   &args,
                                   &ret);
    when_failed_trace(status, exit, ERROR, "Unable to add firewall rule returned %d.", status);
    info->flags.bit.firewall_up = 1;
exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&alias_str);
    free(controller);
    return;
}

/**
 * @brief
 * Deletes the firewall rule to open the DNSSD port on an interface.
 * Calls Firewall.deleteService function.
 *
 * @param info The private data from the interface to open the port on.
 */
void firewall_close_dnssd_port(intf_info_t* info) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* tmp = NULL;
    amxc_string_t alias_str;
    amxd_status_t status = amxd_status_unknown_error;
    char* controller = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&alias_str, 0);

    when_null_trace(info, exit, ERROR, "No private info given for the interface.");

    when_null_trace(info->intf_alias, exit, WARNING, "Unable to get interface alias in disable firewall");

    SAH_TRACEZ_INFO(ME, "Closing firewall service alias %s", info->intf_alias);

    amxc_string_appendf(&alias_str, FW_ALIAS_PREFIX "%s", info->intf_alias);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    tmp = amxc_var_add_new_key(&args, "id");
    amxc_var_push(cstring_t, tmp, amxc_string_take_buffer(&alias_str));

    controller = amxd_object_get_value(cstring_t, info->dnssd, "FWController", NULL);
    when_null_trace(controller, exit, ERROR, "Cannot get controller from pool.");
    status = amxm_execute_function(controller,
                                   FIREWALL_MODULE,
                                   "delete_service",
                                   &args,
                                   &ret);
    when_failed_trace(status, exit, WARNING, "Unable to delete firewall rule.");
    info->flags.bit.firewall_up = 0;
exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&alias_str);
    free(controller);
    return;
}