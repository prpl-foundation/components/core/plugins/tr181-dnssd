/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "dnssd.h"
#include "dnssd_dm_mngr.h"
#include "dnssd_controller.h"
#include "dnssd_info.h"
#include "firewall_utility.h"

#define ME "dnssd"


/**
 * @brief
 * Translate a dnssd mngr action to it's string representation
 *
 * @param action The action/function to be executed on the controller
 * @param args Function arguments
 * @param ret Function return value
 *
 * @return 0 on success
 */
static const cstring_t dnssd_ctrlr_action_str(dnssd_ctrlr_action_t action) {
    switch(action) {
    case DNSSD_CTRLR_DISABLE:
        return "stop-backend";
    case DNSSD_CTRLR_ENABLE:
        return "enable-intfs";
    case DNSSD_CTRLR_RELOAD:
        return "restart-backend";
    }
    return NULL;
}

/**
 * @brief
 * Close or open all intf firewall
 *
 * @param info The dnssd info struct
 */
static void intfs_firewall(dnssd_info_t* info) {
    for(int i = 0; i < info->intfs_size; i++) {
        if((info->flags.bit.enable) &&
           (info->intfs_list[i].flags.bit.firewall_up == 0) &&
           (info->intfs_list[i].flags.bit.intf_alias)) {
            firewall_open_dnssd_port(&(info->intfs_list[i]));
        }

        if(!(info->flags.bit.enable) && (info->intfs_list[i].flags.bit.firewall_up == 1)) {
            firewall_close_dnssd_port(&(info->intfs_list[i]));
        }
    }
}

/**
 * @brief
 * Create the list of listening interface alias.
 *
 * @param info The dnssd info struct
 * @param dest The variant list to fill with listening interface alias.
 */
static void create_alias_list(dnssd_info_t* info, amxc_var_t* dest) {
    amxc_var_t* list_alias = NULL;
    amxc_var_t* list_name = NULL;
    when_null_trace(info, exit, ERROR, "No dnssd info given.");
    when_null_trace(dest, exit, ERROR, "No data to fill.");

    amxc_var_set_type(dest, AMXC_VAR_ID_HTABLE);

    list_alias = amxc_var_add_key(amxc_llist_t, dest, "intf_alias", NULL);
    list_name = amxc_var_add_key(amxc_llist_t, dest, "netdev_name", NULL);

    for(int i = 0; i < info->intfs_size; i++) {
        amxc_var_add(cstring_t, list_alias, info->intfs_list[i].intf_alias);
        amxc_var_add(cstring_t, list_name, info->intfs_list[i].intf_name);
    }
exit:
    return;
}

/**
 * @brief
 * Execute a controller action
 *
 * @param action The action/function to be executed on the controller
 * @param args Function arguments
 * @param ret Function return value
 *
 * @return 0 on success
 */
static int dnssd_ctrlr_execute(const char* action, amxc_var_t* args, amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    char* controller = NULL;
    amxd_object_t* dnssd_obj = amxd_dm_findf(dnssd_get_dm(), "DNSSD.");

    when_null_trace(action, exit, ERROR, "No valid action provided");

    when_null_trace(dnssd_obj, exit, ERROR, "Cannot find DNSSD object");
    controller = amxd_object_get_value(cstring_t, dnssd_obj, "Controller", NULL);
    when_null_trace(controller, exit, ERROR, "Cannot get controller from dnssd.");

    rv = amxm_execute_function(controller,
                               controller,
                               action,
                               args,
                               ret);
    when_failed_trace(rv, exit, ERROR, "Executing controller(%s) function(%s) failed(%d)", controller, action, rv);
exit:
    SAH_TRACEZ_WARNING(ME, "executed: %s(%s) => %d", controller, action, rv);
    free(controller);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief
 * Sync the controller action now
 *
 * @param action The action to be synced to the controller
 */
void dnssd_ctrlr_action(dnssd_ctrlr_action_t action) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* dnssd = amxd_dm_findf(dnssd_get_dm(), "DNSSD.");
    const char* action_str = dnssd_ctrlr_action_str(action);
    dnssd_info_t* info = NULL;
    dnssd_dm_mngr_priv_t* priv_data = NULL;
    int r = -1;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_null_trace(action_str, exit, ERROR, "No valid action provided: %d", action);
    when_null_trace(dnssd, exit, ERROR, "No dnssd object found.");
    priv_data = dnnsd_obj_get_priv(dnssd);
    when_null_trace(priv_data, exit, ERROR, "No private data found for dnssd object.");
    info = dnssd_dm_mngr_get_info(dnssd);
    when_null_trace(info, exit, ERROR, "No private info data found for dnssd object.");

    if((action == DNSSD_CTRLR_ENABLE) || (action == DNSSD_CTRLR_DISABLE)) {
        intfs_firewall(info);
    }

    if(action == DNSSD_CTRLR_ENABLE) {
        create_alias_list(info, &args);
    }

    if(action != DNSSD_CTRLR_DISABLE) {
        when_false_trace(info->flags.bit.intfs_ok, exit, INFO, "Cannot start/reconfigure dnssd as interfaces not yet ok");
        priv_data->back_end_started = true;
    } else {
        priv_data->back_end_started = false;
    }

    r = dnssd_ctrlr_execute(action_str, &args, &ret);

    info->flags.bit.config_ok = (r == 0);
exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    SAH_TRACEZ_OUT(ME);
    return;
}

/**
 * @brief
 * Load a controller
 *
 * @param path The path where the module of the controller can be found.
 * @param controller_name The name of the controller.
 * @return int 0 if loading successful.
 */
static int load_controller(const char* path, const char* controller_name) {
    int retval = 0;
    const amxc_var_t* controller = NULL;
    amxc_string_t mod_path;
    amxm_shared_object_t* so = NULL;
    const char* name = NULL;
    amxd_dm_t* dm = dnssd_get_dm();
    amxd_object_t* dnssd = amxd_dm_findf(dm, "DNSSD.");
    amxo_parser_t* parser = dnssd_get_parser();

    amxc_string_init(&mod_path, 0);

    when_null_trace(path, exit, ERROR, "No path given.");
    when_null_trace(controller_name, exit, ERROR, "No name for controller given.");

    controller = amxd_object_get_param_value(dnssd, controller_name);
    name = GET_CHAR(controller, NULL);
    amxc_string_setf(&mod_path, "%s%s.so", path, name);
    amxc_string_resolve_var(&mod_path, &parser->config);
    retval = amxm_so_open(&so, name, amxc_string_get(&mod_path, 0));
    when_failed_trace(retval, exit, ERROR, "Failed to load %s", amxc_string_get(&mod_path, 0));

exit:
    amxc_string_clean(&mod_path);
    return retval;
}

/**
 * @brief
 * Load all modules' controller.
 *
 * @return int 0 if loading successful.
 */
int dnssd_ctrlr_init(void) {
    int retval = 1;

    retval = load_controller("${mod-path}/", "Controller");
    when_failed_trace(retval, exit, ERROR, "Unable to load server controller.");
    retval = load_controller("/usr/lib/amx/modules/", "FWController");
    when_failed_trace(retval, exit, ERROR, "Unable to load firewall controller.");
exit:
    return retval;
}
