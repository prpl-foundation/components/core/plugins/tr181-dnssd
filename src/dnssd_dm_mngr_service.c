/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "dnssd.h"
#include "dnssd_dm_mngr.h"
#include "dnssd_info.h"

#define ME "dnssd"

/**
 * @brief
 * Add a text entry to the service data model.
 *
 * @param txt The text entry to add.
 * @param service_object The dnssd to add the service to.
 */
static void dm_add_service_text_entry(amxc_var_t* txt,
                                      amxd_object_t* service_object) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_null_trace(txt, exit, ERROR, "No text entry given.");
    when_null_trace(service_object, exit, ERROR, "No service object given.");

    amxd_trans_select_object(&trans, service_object);
    amxd_trans_select_pathf(&trans, ".TextRecord.");
    amxd_trans_add_inst(&trans, 0, NULL);

    amxd_trans_set_value(cstring_t, &trans, "Key", amxc_var_key(txt));
    amxd_trans_set_value(cstring_t, &trans, "Value", GET_CHAR(txt, NULL));

    when_failed_trace(amxd_trans_apply(&trans, dnssd_get_dm()), exit, ERROR, "Cannot add text entry.");
exit:
    amxd_trans_clean(&trans);
    return;
}

/**
 * @brief
 * Update a text entry to the service data model.
 *
 * @param txt The text entry to update.
 * @param service_object The service object to update
 */
static void dm_update_service_text_entry(amxc_var_t* txt,
                                         amxd_object_t* service_object) {
    amxd_object_t* txt_object;
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_null_trace(txt, exit, ERROR, "No text entry given.");

    txt_object = amxd_object_findf(service_object,
                                   "TextRecord.[Key=='%s']",
                                   amxc_var_key(txt));

    amxd_trans_select_object(&trans, txt_object);

    amxd_trans_set_value(cstring_t, &trans, "Value", GET_CHAR(txt, NULL));

    when_failed_trace(amxd_trans_apply(&trans, dnssd_get_dm()), exit, ERROR, "Cannot update text entry.");
exit:
    amxd_trans_clean(&trans);
    return;
}

/**
 * @brief
 * Delete a text entry to the service data model.
 *
 * @param txt The text entry to delete.
 * @param service_object The service object to remove the text entry to.
 */
static void dm_remove_service_text_entry(amxc_var_t* txt,
                                         amxd_object_t* service_object) {
    amxd_object_t* txt_object;
    when_null_trace(txt, exit, WARNING, "No text entry given.");
    txt_object = amxd_object_findf(service_object,
                                   "TextRecord.[Key=='%s']",
                                   amxc_var_key(txt));

    amxd_object_emit_del_inst(txt_object);
    amxd_object_delete(&txt_object);
exit:
    return;
}

/**
 * @brief
 * Fill the DNSSD data model with service parameters
 *
 * @param service The service to add.
 * @param trans transaction to add service to.
 */
static void dm_fill_service_data(amxc_var_t* service, amxd_trans_t* trans) {
    amxc_ts_t time_now;

    amxc_ts_now(&time_now);

    when_null_trace(service, exit, ERROR, "No service given.");
    when_null_trace(trans, exit, ERROR, "No transaction object given.");

    amxd_trans_set_value(cstring_t, trans, "InstanceName", GET_CHAR(service, "InstanceName"));
    amxd_trans_set_value(cstring_t, trans, "ApplicationProtocol", GET_CHAR(service, "ApplicationProtocol"));
    amxd_trans_set_value(cstring_t, trans, "TransportProtocol", GET_CHAR(service, "TransportProtocol"));
    amxd_trans_set_value(cstring_t, trans, "Domain", GET_CHAR(service, "Domain"));
    amxd_trans_set_value(csv_string_t, trans, "Host", GET_CHAR(service, "Host"));
    amxd_trans_set_value(uint32_t, trans, "Port", GET_INT32(service, "Port"));
    if(GET_CHAR(service, "Target") != NULL) {
        amxd_trans_set_value(cstring_t, trans, "Target", GET_CHAR(service, "Target"));
    }
    amxd_trans_set_value(cstring_t, trans, "Status", "LeaseActive");
    amxd_trans_set_value(amxc_ts_t, trans, "LastUpdate", &time_now);
    amxd_trans_set_value(uint32_t, trans, "TimeToLive", UINT32_MAX);
    amxd_trans_set_value(uint32_t, trans, "Priority", 0);
    amxd_trans_set_value(uint32_t, trans, "Weight", 0);
exit:
    return;
}

/**
 * @brief
 * Add a service to the DNSSD data model.
 *
 * @param service The service to add.
 * @param dnnsd The dnssd to add the service to.
 */
static void dm_add_service(amxc_var_t* service,
                           amxd_object_t* dnssd) {
    amxd_object_t* service_object;
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_null_trace(service, exit, ERROR, "No service given.");
    when_null_trace(dnssd, exit, ERROR, "No dnssd object given.");

    amxd_trans_select_object(&trans, dnssd);
    amxd_trans_select_pathf(&trans, ".Service.");
    amxd_trans_add_inst(&trans, 0, NULL);

    dm_fill_service_data(service, &trans);
    when_failed_trace(amxd_trans_apply(&trans, dnssd_get_dm()), exit, ERROR, "Cannot fill datamodel with service.");
    service_object = amxd_object_findf(dnssd,
                                       "Service.[InstanceName=='%s' \
                                                 && ApplicationProtocol=='%s' \
                                                 && TransportProtocol=='%s' \
                                                 && Domain=='%s' \
                                                 && Host=='%s']",
                                       GET_CHAR(service, "InstanceName"),
                                       GET_CHAR(service, "ApplicationProtocol"),
                                       GET_CHAR(service, "TransportProtocol"),
                                       GET_CHAR(service, "Domain"),
                                       GET_CHAR(service, "Host"));
    amxc_var_for_each(entry, GET_ARG(service, "TextRecord")) {
        dm_add_service_text_entry(entry, service_object);
    }
exit:
    amxd_trans_clean(&trans);
    return;
}

/**
 * @brief
 * Check if the text entry is in the list of text entries and remove it from the list.
 *
 * @param txt The text entry to be found.
 * @param list The list of text entries to check.
 * @return true The text entry was found.
 */
static bool check_text_entry_exist(amxc_var_t* txt, amxc_var_t* list) {
    amxc_var_t* txt_found = NULL;
    when_null_trace(txt, exit, ERROR, "No service given.");
    when_null_trace(list, exit, ERROR, "No list given.");

    txt_found = amxc_var_get_key(list, amxc_var_key(txt), AMXC_VAR_FLAG_DEFAULT);
    when_null_trace(txt_found, exit, WARNING, "Text entry not found in list.");
    amxc_var_take_it(txt_found);
    amxc_var_delete(&txt_found);
    return true;
exit:
    return false;
}

/**
 * @brief
 * Update a service to the DNSSD data model.
 *
 * @param service The service to add.
 * @param dnnsd The dnssd to add the service to.
 */
static void dm_update_service(amxc_var_t* service,
                              amxd_object_t* dnssd,
                              amxc_var_t* last_service) {
    amxd_object_t* service_object;
    amxc_var_t* last_txt_list;
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_null_trace(service, exit, ERROR, "No service given.");
    when_null_trace(dnssd, exit, ERROR, "No dnssd object given.");

    service_object = amxd_object_findf(dnssd,
                                       "Service.[InstanceName=='%s' \
                                                 && ApplicationProtocol=='%s' \
                                                 && TransportProtocol=='%s' \
                                                 && Domain=='%s' \
                                                 && Host=='%s']",
                                       GET_CHAR(service, "InstanceName"),
                                       GET_CHAR(service, "ApplicationProtocol"),
                                       GET_CHAR(service, "TransportProtocol"),
                                       GET_CHAR(service, "Domain"),
                                       GET_CHAR(service, "Host"));

    amxd_trans_select_object(&trans, service_object);
    dm_fill_service_data(service, &trans);
    when_failed_trace(amxd_trans_apply(&trans, dnssd_get_dm()), exit, ERROR, "Cannot fill datamodel with service.");
    last_txt_list = GET_ARG(last_service, "TextRecord");

    amxc_var_for_each(txt, GET_ARG(service, "TextRecord")) {
        if((last_txt_list != NULL) && check_text_entry_exist(txt, last_txt_list)) {
            dm_update_service_text_entry(txt, service_object);
        } else {
            dm_add_service_text_entry(txt, service_object);
        }
    }
    amxc_var_for_each(txt, last_txt_list) {
        dm_remove_service_text_entry(txt, service_object);
    }
exit:
    amxd_trans_clean(&trans);
    return;
}

/**
 * @brief
 * Remove a service from the dnssd data model.
 *
 * @param service Service to delete.
 * @param priv dnssd private data.
 * @return int 0 always.
 */
int dm_remove_service(UNUSED amxd_object_t* templ,
                      amxd_object_t* service,
                      UNUSED void* priv) {
    when_null_trace(service, exit, WARNING, "Service to suppress is null.");
    amxd_object_emit_del_inst(service);
    amxd_object_delete(&service);
exit:
    return 0;
}

/**
 * @brief
 * Remove a service from the dnssd data model by finding it from a service variant.
 *
 * @param service Service to delete.
 * @param priv dnssd private data.
 * @return int 0 always.
 */
static int dm_remove_service_with_variant(amxc_var_t* service,
                                          amxd_object_t* dnssd) {
    amxd_object_t* service_object;
    when_null_trace(service, exit, WARNING, "Service to suppress is null.");
    when_null_trace(dnssd, exit, ERROR, "DNSSD object is not given");

    service_object = amxd_object_findf(dnssd,
                                       "Service.[InstanceName=='%s' \
                                                 && ApplicationProtocol=='%s' \
                                                 && TransportProtocol=='%s' \
                                                 && Domain=='%s' \
                                                 && Host=='%s']",
                                       GET_CHAR(service, "InstanceName"),
                                       GET_CHAR(service, "ApplicationProtocol"),
                                       GET_CHAR(service, "TransportProtocol"),
                                       GET_CHAR(service, "Domain"),
                                       GET_CHAR(service, "Host"));

    amxd_object_emit_del_inst(service_object);
    amxd_object_delete(&service_object);
exit:
    return 0;
}

/**
 * @brief
 * Check if the service is in the list of services and return it if present.
 *
 * @param service The service to be found.
 * @param htable The htable of services to check.
 * @return The service removed from the list.
 */
static amxc_var_t* check_service_exist(amxc_var_t* service, amxc_var_t* htable) {
    amxc_var_t* service_found = NULL;
    char buffer_key[1028];
    when_null_trace(service, exit, ERROR, "No service given.");
    when_null_trace(htable, exit, ERROR, "No htable given.");

    snprintf(buffer_key,
             1027,
             "%s/%s/%s/%s/%s",
             GET_CHAR(service, "InstanceName"),
             GET_CHAR(service, "ApplicationProtocol"),
             GET_CHAR(service, "TransportProtocol"),
             GET_CHAR(service, "Domain"),
             GET_CHAR(service, "Host"));

    service_found = amxc_var_get_key(htable, buffer_key, AMXC_VAR_FLAG_DEFAULT);

    when_null_trace(service_found, exit, INFO, "Service not found in list, will be added to the DM.");
exit:
    return service_found;
}

/**
 * @brief
 * add a service to the service htable
 *
 * @param htable The htable to add the service to.
 * @param service The service to add.
 */
static void add_service_htable(amxc_var_t* htable, amxc_var_t* service) {
    char buffer_key[1028];
    amxc_var_t* service_param;
    amxc_var_t* text_entries;

    when_null_trace(htable, exit, ERROR, "No list given.");
    when_null_trace(service, exit, ERROR, "No service given.");

    snprintf(buffer_key,
             1027,
             "%s/%s/%s/%s/%s",
             GET_CHAR(service, "InstanceName"),
             GET_CHAR(service, "ApplicationProtocol"),
             GET_CHAR(service, "TransportProtocol"),
             GET_CHAR(service, "Domain"),
             GET_CHAR(service, "Host"));

    service_param = amxc_var_add_key(amxc_htable_t, htable, buffer_key, NULL);
    amxc_var_add_key(cstring_t, service_param, "InstanceName", GET_CHAR(service, "InstanceName"));
    amxc_var_add_key(cstring_t, service_param, "ApplicationProtocol", GET_CHAR(service, "ApplicationProtocol"));
    amxc_var_add_key(cstring_t, service_param, "TransportProtocol", GET_CHAR(service, "TransportProtocol"));
    amxc_var_add_key(cstring_t, service_param, "Domain", GET_CHAR(service, "Domain"));
    amxc_var_add_key(csv_string_t, service_param, "Host", GET_CHAR(service, "Host"));
    text_entries = amxc_var_add_key(amxc_htable_t, service_param, "TextRecord", NULL);
    amxc_var_for_each(text_entry, GET_ARG(service, "TextRecord")) {
        amxc_var_add_key(cstring_t, text_entries, amxc_var_key(text_entry), "");
    }
exit:
    return;
}

/**
 * @brief
 * Insert services in the DNSSD datamodel, remove first all services.
 *
 * @param function_name Name of the function called.
 * @param args The services to add.
 * @param ret Return variant.
 */
int dm_mngr_set_services(UNUSED const char* function_name,
                         amxc_var_t* args,
                         UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* dnssd = amxd_dm_findf(dnssd_get_dm(), "DNSSD.");
    dnssd_info_t* info = dnssd_dm_mngr_get_info(dnssd);
    amxc_var_t service_htable;

    amxc_var_init(&service_htable);
    amxc_var_set_type(&service_htable, AMXC_VAR_ID_HTABLE);

    when_null_trace(args, exit, ERROR, "Unable get to data from call.");
    when_null_trace(info, exit, ERROR, "No private information found for DNSSD.");

    if(!(info->flags.bit.enable && info->flags.bit.intfs_ok && info->flags.bit.config_ok)) {
        status = amxd_status_ok;
        goto exit;
    }

    amxc_var_for_each(service, args) {
        amxc_var_t* old_service;
        if((info->last_service_htable != NULL) && ((old_service = check_service_exist(service, info->last_service_htable)) != NULL)) {
            dm_update_service(service, info->obj, old_service);
            amxc_var_take_it(old_service);
            amxc_var_delete(&old_service);
        } else {
            dm_add_service(service, info->obj);
        }
        add_service_htable(&service_htable, service);
    }

    amxc_var_for_each(service, info->last_service_htable) {
        dm_remove_service_with_variant(service, dnssd);
    }

    if(info->last_service_htable != NULL) {
        amxc_var_delete(&(info->last_service_htable));
    }
    amxc_var_new(&(info->last_service_htable));
    amxc_var_copy(info->last_service_htable, &service_htable);

    status = amxd_status_ok;
exit:
    amxc_var_clean(&service_htable);
    return status;
}
