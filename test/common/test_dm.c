/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_dm.h>

#include <amxut/amxut_bus.h>
#include <amxut/amxut_timer.h>

#include "test_dm.h"

void test_dm_handle_events(uint32_t ms) {
    amxut_bus_handle_events();
    amxut_timer_go_to_future_ms(ms);
    amxut_bus_handle_events();
    fflush(stdout);
}

static int test_dm_set_param(const char* path, const char* param, amxc_var_t* val) {
    amxd_trans_t trans;
    int rv = -1;

    when_failed(rv = amxd_trans_init(&trans), exit);
    when_failed(rv = amxd_trans_select_pathf(&trans, path), exit);
    when_failed(rv = amxd_trans_set_param(&trans, param, val), exit);
    when_failed(rv = amxd_trans_apply(&trans, amxut_bus_dm()), exit);
exit:
    amxd_trans_clean(&trans);
    return rv;
}

int test_dm_set_param_cstring_t(const char* path, const char* param, const cstring_t value) {
    int rv = -1;
    amxc_var_t val;
    amxc_var_init(&val);
    amxc_var_set(cstring_t, &val, value);

    rv = test_dm_set_param(path, param, &val);

    amxc_var_clean(&val);
    return rv;
}

int test_dm_set_param_csv_string_t(const char* path, const char* param, const csv_string_t value) {
    int rv = -1;
    amxc_var_t val;
    amxc_var_init(&val);
    amxc_var_set(csv_string_t, &val, value);

    rv = test_dm_set_param(path, param, &val);

    amxc_var_clean(&val);
    return rv;
}

int test_dm_set_param_bool(const char* path, const char* param, bool value) {
    int rv = -1;
    amxc_var_t val;
    amxc_var_init(&val);
    amxc_var_set(bool, &val, value);

    rv = test_dm_set_param(path, param, &val);

    amxc_var_clean(&val);
    return rv;
}

void assert_dm_param_equal_cstring_t(const char* path, const char* param, const cstring_t expected, const char* const file, const int line) {
    amxd_object_t* dnssd;
    cstring_t actual = NULL;

    dnssd = amxd_dm_findf(amxut_bus_dm(), path);
    actual = amxd_object_get_value(cstring_t, dnssd, param, NULL);
    _assert_string_equal(actual, expected, file, line);

    free(actual);
}

void assert_dm_param_equal_csv_string_t(const char* path, const char* param, const csv_string_t expected, const char* const file, const int line) {
    amxd_object_t* dnssd;
    cstring_t actual = NULL;

    dnssd = amxd_dm_findf(amxut_bus_dm(), path);
    actual = amxd_object_get_value(cstring_t, dnssd, param, NULL);
    _assert_string_equal(actual, expected, file, line);

    free(actual);
}

void assert_dm_param_equal_bool(const char* path, const char* param, bool expected, const char* const file, const int line) {
    amxd_object_t* dnssd = amxd_dm_findf(amxut_bus_dm(), path);
    bool actual = amxd_object_get_value(bool, dnssd, param, NULL);
    _assert_int_equal(actual, expected, file, line);
}
