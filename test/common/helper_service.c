/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <cmocka.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>

#include "common.h"
#include "helper_service.h"

static amxc_var_t services;

void setup_mod_umdns_services(void) {
    amxc_var_t* service;
    amxc_var_t* text_records;

    amxc_var_init(&services);
    amxc_var_set_type(&services, AMXC_VAR_ID_LIST);

    service = amxc_var_add(amxc_htable_t, &services, NULL);

    amxc_var_add_key(cstring_t, service, "InstanceName", "dummy1");
    amxc_var_add_key(cstring_t, service, "ApplicationProtocol", "printer");
    amxc_var_add_key(cstring_t, service, "TransportProtocol", "UDP");
    amxc_var_add_key(cstring_t, service, "Domain", "dummy_one.com");
    amxc_var_add_key(csv_string_t, service, "Host", "192.168.1.120,fe80::");
    amxc_var_add_key(uint32_t, service, "Port", 505);
    amxc_var_add_key(cstring_t, service, "Target", "dummy_one.local");
    text_records = amxc_var_add_key(amxc_htable_t, service, "TextRecord", NULL);
    amxc_var_add_key(cstring_t, text_records, "Test1", "True");
    amxc_var_add_key(cstring_t, text_records, "Test2", "1234");


    service = amxc_var_add(amxc_htable_t, &services, NULL);

    amxc_var_add_key(cstring_t, service, "InstanceName", "dummy2");
    amxc_var_add_key(cstring_t, service, "ApplicationProtocol", "laser");
    amxc_var_add_key(cstring_t, service, "TransportProtocol", "TCP");
    amxc_var_add_key(cstring_t, service, "Domain", "");
    amxc_var_add_key(csv_string_t, service, "Host", "");
    text_records = amxc_var_add_key(amxc_htable_t, service, "TextRecord", NULL);
}

void update_mod_umdns_services(void) {
    amxc_var_t* service;
    amxc_var_t* text_records;
    amxc_var_t* text_record;

    service = amxc_var_get_index(&services, 0, AMXC_VAR_FLAG_DEFAULT);

    assert_string_equal(GET_CHAR(service, "InstanceName"), "dummy1");

    amxc_var_set(cstring_t, GET_ARG(service, "Target"), "dummy_one.global");
    amxc_var_set(uint32_t, GET_ARG(service, "Port"), 506);

    text_records = GET_ARG(service, "TextRecord");
    text_record = amxc_var_get_key(text_records, "Test1", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_delete(&text_record);
    text_record = amxc_var_get_key(text_records, "Test2", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set(cstring_t, text_record, "5678");
    amxc_var_add_key(cstring_t, text_records, "Test3", "test");


    service = amxc_var_get_index(&services, 1, AMXC_VAR_FLAG_DEFAULT);

    assert_string_equal(GET_CHAR(service, "InstanceName"), "dummy2");

    amxc_var_set(cstring_t, GET_ARG(service, "InstanceName"), "dummy3");
}

void clean_mod_umdns(void) {
    amxc_var_clean(&services);
}

amxc_var_t* get_mod_umdns_services(void) {
    return &services;
}

bool check_module_services(amxd_object_t* services) {
    int counter = 0;
    bool ret = true;

    amxd_object_for_each(instance, it, services) {
        amxc_var_t host_var;
        char* host = NULL;
        amxd_object_t* service = amxc_container_of(it, amxd_object_t, it);
        amxd_object_t* texts = amxd_object_findf(service, ".TextRecord.");
        char* app_prot = amxd_object_get_value(cstring_t, service, "ApplicationProtocol", NULL);
        char* trans_prot = amxd_object_get_value(cstring_t, service, "TransportProtocol", NULL);
        char* inst_name = amxd_object_get_value(cstring_t, service, "InstanceName", NULL);
        char* domain = amxd_object_get_value(cstring_t, service, "Domain", NULL);
        char* target = amxd_object_get_value(cstring_t, service, "Target", NULL);
        amxc_var_init(&host_var);
        amxd_object_get_param(service, "Host", &host_var);
        host = amxc_var_dyncast(csv_string_t, &host_var);

        counter++;
        switch(amxd_object_get_value(uint32_t, service, "Port", NULL)) {
        case 505:;
            int inner_counter = 0;
            assert_string_equal(app_prot, "printer");
            assert_string_equal(trans_prot, "UDP");
            assert_string_equal(inst_name, "dummy1");
            assert_string_equal(domain, "dummy_one.com");
            assert_string_equal(target, "dummy_one.local");
            assert_string_equal(host, "192.168.1.120,fe80::");
            amxd_object_for_each(instance, it, texts) {
                amxd_object_t* txt = amxc_container_of(it, amxd_object_t, it);
                char* key = amxd_object_get_value(cstring_t, txt, "Key", NULL);
                char* value = amxd_object_get_value(cstring_t, txt, "Value", NULL);

                if(strcmp(key, "Test1") == 0) {
                    assert_string_equal(value, "True");
                    inner_counter++;
                }
                if(strcmp(key, "Test2") == 0) {
                    assert_string_equal(value, "1234");
                    inner_counter++;
                }
                free(key);
                free(value);
            }
            assert_int_equal(inner_counter, 2);
            break;
        case 0:
            assert_string_equal(app_prot, "laser");
            assert_string_equal(trans_prot, "TCP");
            assert_string_equal(inst_name, "dummy2");
            assert_string_equal(domain, "");
            assert_string_equal(host, "");
            break;
        default:
            ret = false;
            break;
        }

        free(app_prot);
        free(trans_prot);
        free(inst_name);
        free(domain);
        free(target);
        amxc_var_clean(&host_var);
        free(host);
    }
    assert_int_equal(counter, 2);
    return ret;
}

bool check_module_services_updated(amxd_object_t* services) {
    int counter = 0;
    bool ret = true;

    amxd_object_for_each(instance, it, services) {
        amxc_var_t host_var;
        char* host = NULL;
        amxd_object_t* service = amxc_container_of(it, amxd_object_t, it);
        amxd_object_t* texts = amxd_object_findf(service, ".TextRecord.");
        char* app_prot = amxd_object_get_value(cstring_t, service, "ApplicationProtocol", NULL);
        char* trans_prot = amxd_object_get_value(cstring_t, service, "TransportProtocol", NULL);
        char* inst_name = amxd_object_get_value(cstring_t, service, "InstanceName", NULL);
        char* domain = amxd_object_get_value(cstring_t, service, "Domain", NULL);
        char* target = amxd_object_get_value(cstring_t, service, "Target", NULL);
        amxc_var_init(&host_var);
        amxd_object_get_param(service, "Host", &host_var);
        host = amxc_var_dyncast(csv_string_t, &host_var);

        counter++;
        switch(amxd_object_get_value(uint32_t, service, "Port", NULL)) {
        case 506:;
            int inner_counter = 0;
            assert_string_equal(app_prot, "printer");
            assert_string_equal(trans_prot, "UDP");
            assert_string_equal(inst_name, "dummy1");
            assert_string_equal(domain, "dummy_one.com");
            assert_string_equal(target, "dummy_one.global");
            assert_string_equal(host, "192.168.1.120,fe80::");
            amxd_object_for_each(instance, it, texts) {
                amxd_object_t* txt = amxc_container_of(it, amxd_object_t, it);
                char* key = amxd_object_get_value(cstring_t, txt, "Key", NULL);
                char* value = amxd_object_get_value(cstring_t, txt, "Value", NULL);

                if(strcmp(key, "Test1") == 0) {
                    inner_counter++;
                }
                if(strcmp(key, "Test2") == 0) {
                    assert_string_equal(value, "5678");
                    inner_counter++;
                }
                if(strcmp(key, "Test3") == 0) {
                    assert_string_equal(value, "test");
                    inner_counter++;
                }
                free(key);
                free(value);
            }
            assert_int_equal(inner_counter, 2);
            break;
        case 0:
            assert_string_equal(app_prot, "laser");
            assert_string_equal(trans_prot, "TCP");
            assert_string_equal(inst_name, "dummy3");
            assert_string_equal(domain, "");
            assert_string_equal(host, "");
            break;
        default:
            ret = false;
            break;
        }

        free(app_prot);
        free(trans_prot);
        free(inst_name);
        free(domain);
        free(target);
        amxc_var_clean(&host_var);
        free(host);
    }
    assert_int_equal(counter, 2);
    return ret;
}

void add_service(amxd_object_t* dnssd, const char* instance_name, const char* app_proto, const char* trans_proto) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, dnssd);
    amxd_trans_select_pathf(&trans, ".Service.");

    amxd_trans_add_inst(&trans, 0, instance_name);
    amxd_trans_set_value(cstring_t, &trans, "InstanceName", instance_name);
    amxd_trans_set_value(cstring_t, &trans, "ApplicationProtocol", app_proto);
    amxd_trans_set_value(cstring_t, &trans, "TransportProtocol", trans_proto);
    amxd_trans_set_value(cstring_t, &trans, "Domain", "dummy-domain");
    amxd_trans_set_value(csv_string_t, &trans, "Host", "dummy-host");

    amxd_trans_select_pathf(&trans, ".TextRecord.");

    amxd_trans_add_inst(&trans, 0, "dummy_record");
    amxd_trans_set_value(cstring_t, &trans, "Key", "Test");
    amxd_trans_set_value(cstring_t, &trans, "Value", "Hello");

    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), amxd_status_ok);
    amxd_trans_clean(&trans);
}
