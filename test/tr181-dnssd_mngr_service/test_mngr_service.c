/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <cmocka.h>

#include <amxc/amxc_macros.h>

#include <amxut/amxut_bus.h>

#include "../common/common.h"
#include "../common/helper_service.h"
#include "../common/test_dm.h"

#include "test_mngr_service.h"
#include "dnssd_dm_mngr.h"

static int count_number_of_services(amxd_object_t* services_object) {
    int counter = 0;
    amxd_object_for_each(instance, it, services_object) {
        counter++;
    }
    return counter;
}

void test_set_remove_services(UNUSED void** state) {
    amxd_object_t* dnssd = amxd_dm_findf(amxut_bus_dm(), "DNSSD.");
    amxd_object_t* services_object = amxd_dm_findf(amxut_bus_dm(), "DNSSD.Service.");
    amxc_var_t* services = get_mod_umdns_services();

    assert_dm_param_equal(cstring_t, "DNSSD.", "Status", "Enabled");

    setup_mod_umdns_services();
    dm_mngr_set_services(NULL, services, NULL);
    check_module_services(services_object);
    assert_int_equal(count_number_of_services(services_object), 2);

    update_mod_umdns_services();
    dm_mngr_set_services(NULL, services, NULL);
    check_module_services_updated(services_object);
    assert_int_equal(count_number_of_services(services_object), 2);

    amxd_object_for_all(services_object, "*", dm_remove_service, dnssd->priv);
    assert_int_equal(count_number_of_services(services_object), 0);

    clean_mod_umdns();
}
