/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_transaction.h>

#include <amxut/amxut_bus.h>

#include "../common/common.h"
#include "../common/test_dm.h"
#include "../mocks/mock_amxm.h"

#include "test_mngr.h"
#include "dnssd.h"
#include "dnssd_dm_mngr.h"

#define ME "test-mngr"

static void configure_dnssd(bool enable, const char* advertise_intfs) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNSSD.");
    amxd_trans_set_value(bool, &trans, "Enable", enable);
    amxd_trans_set_value(csv_string_t, &trans, "AdvertisedInterfaces", advertise_intfs);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), amxd_status_ok);

    amxd_trans_clean(&trans);

    test_dm_handle_events(1000);
}

void test_enable_disable(UNUSED void** state) {
    amxd_object_t* dnssd = amxd_dm_findf(amxut_bus_dm(), "DNSSD.");

    assert_dm_set_param_and_sync(bool, "DNSSD.", "Enable", true);
    assert_non_null(dnssd->priv);
    assert_dm_param_equal(csv_string_t, "DNSSD.", "AdvertisedInterfaces", "Device.IP.Interface.3.");
    assert_dm_param_equal(cstring_t, "DNSSD.", "Status", "Enabled");

    expect_amxm_execute_function_umdns("stop-backend");
    assert_dm_set_param_and_sync(bool, "DNSSD.", "Enable", false);
    assert_non_null(dnssd->priv);
    assert_dm_param_equal(csv_string_t, "DNSSD.", "AdvertisedInterfaces", "Device.IP.Interface.3.");
    assert_dm_param_equal(cstring_t, "DNSSD.", "Status", "Disabled");

    expect_amxm_execute_function_umdns("enable-intfs");
    assert_dm_set_param_and_sync(bool, "DNSSD.", "Enable", true);
    assert_non_null(dnssd->priv);
    assert_dm_param_equal(csv_string_t, "DNSSD.", "AdvertisedInterfaces", "Device.IP.Interface.3.");
    assert_dm_param_equal(cstring_t, "DNSSD.", "Status", "Enabled");
}

void test_sync_intfs_status(UNUSED void** state) {
    assert_dm_param_equal(cstring_t, "DNSSD.", "Status", "Enabled");

    expect_amxm_execute_function_umdns("stop-backend");
    assert_dm_set_param_and_sync(csv_string_t, "DNSSD.", "AdvertisedInterfaces", "test");
    assert_dm_param_equal(cstring_t, "DNSSD.", "Status", "Error_Misconfigured");

    // don't expect the back-end to stop as already stopped, so no call to back-end should be done
    assert_dm_set_param_and_sync(bool, "DNSSD.", "Enable", false);
    assert_dm_param_equal(cstring_t, "DNSSD.", "Status", "Disabled");

    expect_amxm_execute_function_umdns("enable-intfs");
    configure_dnssd(true, "Device.IP.Interface.3.");
    assert_dm_param_equal(cstring_t, "DNSSD.", "Status", "Enabled");
}

void test_restart_backend(UNUSED void** state) {
    assert_dm_param_equal(cstring_t, "DNSSD.", "Status", "Enabled");

    dnssd_dm_mngr_restart(NULL, NULL, NULL);

    expect_amxm_execute_function_umdns("stop-backend");
    expect_amxm_execute_function_umdns("enable-intfs");
    test_dm_handle_events(1000);
    assert_dm_param_equal(cstring_t, "DNSSD.", "Status", "Enabled");
}

void test_restart_backend_only_triggers_once(UNUSED void** state) {
    assert_dm_param_equal(cstring_t, "DNSSD.", "Status", "Enabled");

    dnssd_dm_mngr_restart(NULL, NULL, NULL);
    dnssd_dm_mngr_restart(NULL, NULL, NULL);
    dnssd_dm_mngr_restart(NULL, NULL, NULL);

    expect_amxm_execute_function_umdns("stop-backend");
    expect_amxm_execute_function_umdns("enable-intfs");
    test_dm_handle_events(1000);
    assert_dm_param_equal(cstring_t, "DNSSD.", "Status", "Enabled");
}

void test_restart_backend_has_no_effect_if_starting(UNUSED void** state) {
    assert_dm_param_equal(cstring_t, "DNSSD.", "Status", "Enabled");

    // Fully disable
    expect_amxm_execute_function_umdns("stop-backend");
    assert_dm_set_param_and_sync(bool, "DNSSD.", "Enable", false);
    assert_dm_param_equal(cstring_t, "DNSSD.", "Status", "Disabled");

    // Request enable (no sync time)
    assert_dm_set_param(bool, "DNSSD.", "Enable", true);
    assert_dm_param_equal(cstring_t, "DNSSD.", "Status", "Disabled");

    dnssd_dm_mngr_restart(NULL, NULL, NULL);

    expect_amxm_execute_function_umdns("enable-intfs");
    test_dm_handle_events(1000);
    assert_dm_param_equal(cstring_t, "DNSSD.", "Status", "Enabled");
}
