/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include "mock_amxb.h"

static amxb_bus_ctx_t bus;
static int local_index = 0;
static char path[16];

amxb_bus_ctx_t* __wrap_amxb_be_who_has(UNUSED const char* path) {
    return &bus;
}

int __wrap_amxb_subscribe(amxb_bus_ctx_t* ctx, const char* object, const char* expression, amxp_slot_fn_t slot_cb, void* priv) {
    amxc_var_t data;

    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    assert_int_equal(ctx, &bus);
    assert_string_equal(object, "DNSSD.Service.");
    assert_non_null(slot_cb);
    assert_non_null(expression);

    amxc_var_add_key(uint32_t, &data, "index", local_index);
    amxc_var_add_key(cstring_t, &data, "path", path);
    amxc_var_add_key(cstring_t, &data, "notification", "dm:instance-added");

    slot_cb(NULL, &data, priv);

    amxc_var_clean(&data);
    return 0;
}

int __wrap_amxb_unsubscribe(amxb_bus_ctx_t* ctx,
                            const char* object,
                            amxp_slot_fn_t slot_cb,
                            UNUSED void* info) {
    assert_int_equal(ctx, &bus);
    assert_string_equal(object, "DNSSD.Service.");
    assert_non_null(slot_cb);
    return 0;
}

void mock_amxb_set_event_index(int index) {
    local_index = index;
    return;
}

void mock_amxb_set_event_path(char* src) {
    memset(path, '\0', sizeof(path));
    strcpy(path, src);
    return;
}