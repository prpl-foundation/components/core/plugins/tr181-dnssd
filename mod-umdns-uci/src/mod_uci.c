/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_uci.h"
#include "ubus_uci.h"
#include "umdns/common_api.h"

#define ME "dnssd_uci"

struct _mod_uci {
    amxm_shared_object_t* so;
};

static struct _mod_uci uci;

/**
 * @brief
 * Enable the listening of MDNS messages on the selected interfaces.
 * This remove all interface that where not listed.
 *
 * @param function_name Name of the function called.
 * @param args list of listening interfaces Alias.
 * @param ret Return variant.
 * @return int 0 on success.
 */
int enable_dnssd_intfs_uci(UNUSED const char* function_name,
                           amxc_var_t* args,
                           UNUSED amxc_var_t* ret) {
    int retval = 1;
    bool delete = false;
    const amxc_llist_t* intfs_list;
    amxc_var_t uci_args;
    amxc_var_t ret_val;

    amxc_var_init(&ret_val);
    amxc_var_init(&uci_args);
    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);

    when_null_trace(args, exit, ERROR, "Unable to get list of listening interfaces.");

    intfs_list = amxc_var_constcast(amxc_llist_t, GET_ARG(args, "intf_alias"));
    amxc_var_add_key(amxc_llist_t, &uci_args, "network", intfs_list);
    delete = amxc_llist_size(intfs_list) == 0;
    if(delete) {
        retval = uci_call("delete", "umdns", "umdns", NULL, &ret_val);
        when_failed_trace(retval, exit, WARNING, "Unable to delete umdns entry in uci.");
    } else {
        retval = uci_call("set", "umdns", "umdns", &uci_args, &ret_val);
        if(retval != 0) {
            retval = uci_call("add", "umdns", "umdns", &uci_args, &ret_val);
            when_failed_trace(retval, exit, ERROR, "Unable to set umdns entry in uci.");
        }
    }
    retval = uci_call("commit", "umdns", NULL, NULL, &ret_val);
    when_failed_trace(retval, exit, ERROR, "Unable to commit change to uci.");
exit:
    amxc_var_clean(&ret_val);
    amxc_var_clean(&uci_args);
    return retval;
}

static int restart_backend(UNUSED const char* function_name,
                           UNUSED amxc_var_t* args,
                           UNUSED amxc_var_t* ret) {
    int rc = 1;
    amxp_subproc_t* subproc = NULL;
    const char* argument[] = {NULL, NULL};
    argument[0] = UMDNS_INITD;
    argument[1] = "restart";

    amxp_subproc_new(&subproc);

    SAH_TRACEZ_INFO(ME, "Calling %s %s", argument[0], argument[1]);
    rc = amxp_subproc_vstart_wait(subproc, 1000, (char**) argument);
    switch(rc) {
    case -1:
        SAH_TRACEZ_ERROR(ME, "Error calling umdns init.d script");
        break;
    case 1:
        SAH_TRACEZ_ERROR(ME, "Killing umdns init.d script that timed out");
        amxp_subproc_kill(subproc, SIGKILL);
    }

    amxp_subproc_delete(&subproc);
    return rc;
}

static int stop_backend(UNUSED const char* function_name,
                        UNUSED amxc_var_t* args,
                        UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_WARNING(ME, "Not stopping umdns when controlled with uci");
    return 0;
}

/**
 * @brief
 * Send a services to the dnssd plugin.
 *
 * @param services the list of services to add.
 */
static void send_services(amxc_var_t* services) {
    amxc_var_t rv;
    int status = 1;

    amxc_var_init(&rv);

    when_null_trace(services, exit, ERROR, "No services to send to the plugin.");

    status = amxm_execute_function("self",
                                   MOD_DM_MNGR,
                                   "set-service",
                                   services,
                                   &rv);

    when_failed_trace(status, exit, ERROR, "Could not send the lease to the plugin %d.", status);
exit:
    amxc_var_clean(&rv);
    return;
}

/**
 * @brief
 * Start and register functions for the uci module.
 *
 * @return AMXM_CONSTRUCTOR always return 0.
 */
static AMXM_CONSTRUCTOR dnssd_uci_start(void) {
    amxm_module_t* mod = NULL;
    uci.so = amxm_so_get_current();
    amxm_module_register(&mod, uci.so, MOD_UMDNS_UCI);
    amxm_module_add_function(mod, "stop-backed", stop_backend);
    amxm_module_add_function(mod, "enable-intfs", enable_dnssd_intfs_uci);
    amxm_module_add_function(mod, "restart-backend", restart_backend);
    umdns_init(send_services);
    return 0;
}

/**
 * @brief
 * Stop and clean the uci module.
 *
 * @return AMXM_DESTRUCTOR always return 0.
 */
static AMXM_DESTRUCTOR dnssd_uci_stop(void) {
    uci.so = NULL;
    umdns_clean();
    return 0;
}
