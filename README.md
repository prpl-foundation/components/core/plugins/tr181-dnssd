# DNS services discovery plugin
[[_TOC_]]

## Using DNS service discovery

To use this plugin, [umdns](https://openwrt.org/docs/guide-developer/mdns) need to be installed on the board. As for now the only mdns backend that the plugin support is umdns.

## compile and install

```
make
sudo make install
```

## run test

```
make test
```

example of test output ( 21/12/2022 ):
```
------------------------------------------------------------------------------
                           GCC Code Coverage Report
Directory: ../../..
------------------------------------------------------------------------------
File                                       Lines    Exec  Cover   Missing
------------------------------------------------------------------------------
mod-umdns/test/common_umdns/mock_amxm_umdns.c
                                              16      16   100%   
libumdns/test/common_libumdns/mock_amxb_libumdns.c
                                              21      21   100%   
libumdns/test/common_libumdns/common_libumdns.c
                                              51      51   100%   
mod-umdns-uci/test/common_uci/mock_amxm_uci.c
                                              16      16   100%   
mod-umdns/test/common_umdns/mock_libumdns_umdns.c
                                               9       9   100%   
mod-umdns/test/common_umdns/mock_amxp_umdns.c
                                              16      16   100%   
mod-umdns-uci/src/mod_uci.c                   43      43   100%   
mod-umdns-uci/src/ubus_uci.c                  16      16   100%   
test/common/common.c                          33      33   100%   
libumdns/src/common_api.c                    12      12   100%   
src/firewall_utility.c                        50      50   100%   
mod-umdns-uci/test/common_uci/mock_libumdns_uci.c
                                               9       9   100%   
mod-umdns-uci/test/common_uci/common_uci.c
                                              30      30   100%   
src/dnssd_main.c                              27      27   100%   
mod-umdns/test/common_umdns/common_umdns.c
                                              30      30   100%   
src/dnssd_dm_mngr.c                          142     141    99%   242
src/dnssd_dm_mngr_service.c                  186     184    98%   445-446
libumdns/src/umdns_utils.c                  112     110    98%   184,199
src/dm_dnssd_methode.c                        55      54    98%   163
src/dnssd_info.c                             183     178    97%   147-148,151-152,426
test/common/helper_service.c                 169     162    95%   190-192,236,258-260
mod-umdns-uci/test/common_uci/mock_amxb_uci.c
                                              65      61    93%   88,93,127-128
src/dm_dnssd_events.c                         79      73    92%   205,209,222-223,229-230
mod-umdns/src/mod_umdns.c                     74      66    89%   124,147-153
------------------------------------------------------------------------------
TOTAL                                       1444    1408    97%
------------------------------------------------------------------------------
lines: 97.5% (1408 out of 1444)
branches: 22.4% (365 out of 1631)
```

## run plugin inside a Ambiorix Docker container

Refer to the Ambiorix tutorials for the steps to launch the Ambiorix Docker container.

### compile/install dependencies: (make + sudo make install)

 * libsahtrace
 * libnetmodel
 * libamxb
 * libamxc
 * libamxd
 * libamxm
 * libamxo
 * libamxp

### run dependencies

 * umdns

### activate tr181-dnssd plugin
```
/etc/init.d/tr181-dnssd start
```

## tips & trick
### Resolve a service on the listening interfaces
To resolve a service on the listening interfaces, you first need to add a client on one of those interfaces. Then your client also need to advertise a service on the network.

The service advertiser that can be used is [avahi](https://www.avahi.org/doxygen/html/). Then a service can be added in the service repository ( depending of your distribution ). One example of service can be found below.
```
<service-group>
  <name replace-wildcards="yes">test</name>
  <service>
    <type>_pdf._udp</type>
    <domain-name>local</domain-name>
    <port>5904</port>
    <txt-record>thas=as</txt-record>
    <txt-record>this=is</txt-record>
  </service>
</service-group>
```

Then after severals second, the service will be listed in DNSSD.Service.
