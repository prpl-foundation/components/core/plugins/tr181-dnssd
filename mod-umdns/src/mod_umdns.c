/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxm/amxm.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxb/amxb.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "umdns/common_api.h"
#include "mod_umdns.h"
#include "netmodel/client.h"

#define ME "mod_umdns"
#define TIMEOUT_IN_SEC 10

typedef enum _umdns_state {
    STOPPED,
    STARTING,
    RUNNING
} umdns_state_t;
struct _mod_umdns {
    amxm_shared_object_t* so;
    amxp_subproc_t* subproc;
    amxc_var_t* intf_list;
    umdns_state_t state;
    amxp_timer_t* retry_who_has_timer;
    bool slot_ok;
};

static struct _mod_umdns umdns;

static int start_umdns(void);

/**
 * @brief
 * Send a lease to the DHCPv4 plugin.
 *
 * @param lease the lease to add.
 */
static void send_services(amxc_var_t* services) {
    amxc_var_t rv;
    int status = 1;

    when_null_trace(services, exit, ERROR, "No services to send to the plugin.");

    amxc_var_init(&rv);

    status = amxm_execute_function("self",
                                   MOD_DM_MNGR,
                                   "set-service",
                                   services,
                                   &rv);

    amxc_var_clean(&rv);

    when_failed_trace(status, exit, ERROR, "Could not send the lease to the plugin %d.", status);
exit:
    return;
}

/**
 * @brief
 * Callback when umdns stop
 *
 * @param event_name Name of the event send.
 * @param event_data Date of the event.
 * @param priv NULL.
 */
static void umdns_stopped(UNUSED const char* const event_name,
                          const amxc_var_t* const event_data,
                          UNUSED void* const priv) {
    uint32_t exit_code = GET_UINT32(event_data, "ExitCode");
    uint32_t sig_code = GET_UINT32(event_data, "Signal");
    // this event handler is always called, even on expected restart, only handle the error cases with this function
    if(exit_code != 0) {
        SAH_TRACEZ_ERROR(ME, "Umdns stopped with code %d!", exit_code);
        umdns.state = STOPPED;
    }
    if(sig_code != 0) {
        SAH_TRACEZ_ERROR(ME, "dnsmasq crash unexpected, restarting it, signal : %d", sig_code);
        umdns.state = STOPPED;
        start_umdns();
    }
}

/**
 * @brief
 * Try to get back the umdns bus
 *
 * @param local_timer The timer that call back this function.
 * @param priv private information pass through calls.
 */
static void umdns_retry_get_umdns_bus(UNUSED amxp_timer_t* local_timer,
                                      UNUSED void* priv) {
    amxc_var_t args;
    amxc_var_t result;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("umdns.");
    int retval = 1;

    when_null_trace(ctx, exit, ERROR, "Cannot get the bus controlling umdns.");

    amxc_var_init(&args);
    amxc_var_init(&result);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(amxc_llist_t, &args, "interfaces", amxc_var_constcast(amxc_llist_t, umdns.intf_list));

    retval = amxb_call(ctx, "umdns.", "set_config", &args, &result, TIMEOUT_IN_SEC);
    amxc_var_clean(&args);
    amxc_var_clean(&result);
    when_failed_trace(retval, exit, ERROR, "Cannot delayed call umdns.set_config");
    umdns.state = RUNNING;
exit:
    return;
}

/**
 * @brief
 * Call back function to send interfaces to umdns when not yet on the bus.
 *
 * @param signame name of the signal received.
 * @param data data from the signal.
 * @param priv data passed by the creator of the call back.
 */
static void umdns_interfaces_cb(UNUSED const char* signame,
                                UNUSED const amxc_var_t* const data,
                                UNUSED void* const priv) {
    amxc_var_t args;
    amxc_var_t result;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("umdns.");
    int retval = 1;

    if(ctx == NULL) {
        amxp_timer_start(umdns.retry_who_has_timer, 1000);
        SAH_TRACEZ_WARNING(ME, "Retry to get umdns bus");
        goto exit;
    }

    amxc_var_init(&args);
    amxc_var_init(&result);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(amxc_llist_t, &args, "interfaces", amxc_var_constcast(amxc_llist_t, umdns.intf_list));

    retval = amxb_call(ctx, "umdns.", "set_config", &args, &result, TIMEOUT_IN_SEC);
    amxc_var_clean(&args);
    amxc_var_clean(&result);
    when_failed_trace(retval, exit, ERROR, "Cannot delayed call umdns.set_config");
    umdns.state = RUNNING;
exit:
    return;
}

/**
 * @brief
 * Set the interfaces of umdns
 *
 * @return int 0 on success
 */
static int set_interfaces_umdns(void) {
    amxc_var_t args;
    amxc_var_t result;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("umdns.");
    int retval = 1;

    if(umdns.state != RUNNING) {
        if(!umdns.slot_ok) {
            retval = amxp_slot_connect_filtered(NULL, "^wait:umdns\\.$", NULL, umdns_interfaces_cb, NULL);
            when_failed_trace(retval, exit, ERROR, "Failed to wait for umdns to be available");
            umdns.slot_ok = true;
        }
        retval = amxb_wait_for_object("umdns.");
        if(retval != AMXB_STATUS_OK) {
            SAH_TRACEZ_ERROR(ME, "cannot wait for umdns retrying %d", retval);
            retval = amxb_wait_for_object("umdns.");
            when_failed_trace(retval, exit, ERROR, "retry failed %d", retval);
        }
    } else {
        amxc_var_init(&args);
        amxc_var_init(&result);

        amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

        amxc_var_add_key(amxc_llist_t, &args, "interfaces", amxc_var_constcast(amxc_llist_t, umdns.intf_list));

        retval = amxb_call(ctx, "umdns.", "set_config", &args, &result, TIMEOUT_IN_SEC);
        amxc_var_clean(&args);
        amxc_var_clean(&result);
        when_failed_trace(retval, exit, ERROR, "Failed to call umdns.set_config()");
    }
exit:
    return retval;
}

/**
 * @brief
 * Start umdns utility with no arguments.
 *
 * @return int 0 on success
 */
static int start_umdns(void) {
    SAH_TRACEZ_IN(ME);
    int retval = 1;
    const char* cmd[] = {"/usr/sbin/umdns", "&", NULL};
    static uint32_t count = 0;

    when_null_trace(umdns.subproc, exit, ERROR, "No umdns process");

    if(amxp_subproc_is_running(umdns.subproc)) {
        SAH_TRACEZ_INFO(ME, "umdns already running");
        retval = 0;
        goto exit;
    }

    retval = set_interfaces_umdns();
    when_failed_trace(retval, exit, ERROR, "Cannot set interfaces");

    amxp_slot_connect(umdns.subproc->sigmngr, "stop", NULL, umdns_stopped, NULL);
    retval = amxp_subproc_vstart(umdns.subproc, (char**) cmd);
    when_false_trace(retval == 0, exit, ERROR, "Failed to start umdns");

    umdns.state = STARTING;

    count++;
    SAH_TRACEZ_INFO(ME, "Started umdns (%u times)", count);

    umdns_init(send_services);
exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

/**
 * @brief
 * Stop the umdns utility.
 *
 * @param function_name Name of the function called
 * @param data NULL
 * @param ret NULL
 * @return int 0 on success
 */
int stop_umdns(UNUSED const char* function_name,
               UNUSED amxc_var_t* data,
               UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int retval = 1;
    static uint32_t count = 0;

    when_null_trace(umdns.subproc, exit, ERROR, "Could not find the subproc");

    if(!amxp_subproc_is_running(umdns.subproc)) {
        SAH_TRACEZ_INFO(ME, "umdns already stopped");
        retval = 0;
        goto exit;
    }

    umdns.state = STOPPED;
    amxp_slot_disconnect(umdns.subproc->sigmngr, "stop", umdns_stopped);
    retval = amxp_subproc_kill(umdns.subproc, SIGTERM);
    if(retval != 0) {
        SAH_TRACEZ_WARNING(ME, "Failed to kill the subproc with SIGTERM %s (%d) %d", strerror(errno), errno, retval);
    }
    retval = amxp_subproc_wait(umdns.subproc, TIMEOUT_IN_SEC * 1000);
    if(retval != 0) {
        SAH_TRACEZ_WARNING(ME, "Time out to SIGTERM umdns sending SIGKILL");
        retval = amxp_subproc_kill(umdns.subproc, SIGKILL);
        when_failed_trace(retval, exit, ERROR, "Failed to kill the subproc with SIGKILL %s (%d) %d", strerror(errno), errno, retval);
        retval = amxp_subproc_wait(umdns.subproc, TIMEOUT_IN_SEC * 1000);
        when_failed_trace(retval, exit, ERROR, "SIGKILL timed out.");
    }
    count++;
    SAH_TRACEZ_INFO(ME, "Stopped umdns (%u times)", count);
    umdns_clean();
exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

/**
 * @brief
 * Enable the listening of MDNS messages on the selected interfaces.
 * This remove all interface that where not listed.
 *
 * @param function_name Name of the function called.
 * @param args list of listening interfaces Alias.
 * @param ret Return variant.
 * @return int 0 on success.
 */
int enable_dnssd_intfs_umdns(UNUSED const char* function_name,
                             amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    int rc = 1;

    when_null_trace(args, exit, ERROR, "Unable to get list of listening interfaces.");
    if(umdns.intf_list != NULL) {
        amxc_var_delete(&umdns.intf_list);
    }
    umdns.intf_list = amxc_var_get_key(args, "netdev_name", AMXC_VAR_FLAG_COPY);

    SAH_TRACEZ_INFO(ME, "Starting umdns as not yet started: %d", umdns.state);
    rc = start_umdns();
exit:
    return rc;
}

/**
 * @brief
 * Call the reload function of umdns or start it if not yet started
 *
 * @param function_name Name of the function called
 * @param data NULL
 * @param ret NULL
 * @return int 0 when successfully started
 */
int restart_umdns(UNUSED const char* function_name,
                  UNUSED amxc_var_t* data,
                  UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rc = 1;

    if(umdns.state == RUNNING) {
        amxc_var_t args;
        amxc_var_t result;
        amxb_bus_ctx_t* ctx = amxb_be_who_has("umdns.");

        when_null_trace(ctx, exit, ERROR, "Cannot get the bus controlling umdns.");

        amxc_var_init(&args);
        amxc_var_init(&result);

        rc = amxb_call(ctx, "umdns.", "reload", &args, &result, TIMEOUT_IN_SEC);
        amxc_var_clean(&args);
        amxc_var_clean(&result);
    } else {
        SAH_TRACEZ_INFO(ME, "Not reloading as umdns not yet started");
        rc = 0;
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return rc;
}

/**
 * @brief
 * Start and register functions for the uci module.
 *
 * @return AMXM_CONSTRUCTOR always return 0.
 */
static AMXM_CONSTRUCTOR dnssd_umdns_start(void) {
    amxm_module_t* mod = NULL;
    umdns.so = amxm_so_get_current();
    umdns.state = STOPPED;
    umdns.slot_ok = false;
    amxc_var_new(&umdns.intf_list);
    amxp_subproc_new(&umdns.subproc);
    amxm_module_register(&mod, umdns.so, MOD_UMDNS);
    amxm_module_add_function(mod, "stop-backend", stop_umdns);
    amxm_module_add_function(mod, "enable-intfs", enable_dnssd_intfs_umdns);
    amxm_module_add_function(mod, "restart-backend", restart_umdns);
    amxp_timer_new(&umdns.retry_who_has_timer, umdns_retry_get_umdns_bus, NULL);
    return 0;
}

/**
 * @brief
 * Stop and clean the uci module.
 *
 * @return AMXM_DESTRUCTOR always return 0.
 */
static AMXM_DESTRUCTOR dnssd_umdns_stop(void) {
    umdns.so = NULL;
    amxp_timer_delete(&umdns.retry_who_has_timer);
    amxp_slot_disconnect_all(umdns_interfaces_cb);
    amxp_slot_disconnect_all(umdns_stopped);
    amxp_subproc_delete(&umdns.subproc);
    amxc_var_delete(&umdns.intf_list);
    umdns_clean();
    return 0;
}
